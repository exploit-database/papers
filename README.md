# Exploit Database's Papers

The papers are located in the `/docs/` (binary) and `/papers/` (plain-text) directories. An [index](https://gitlab.com/exploit-database/exploitdb-papers/blob/master/files_papers.csv) of the paper archives can be found in `/files_papers.csv`. _Note: We have not included any eZines in this offline archive ([1](https://www.exploit-db.com/ezines/) & [2](https://www.exploit-db.com/?type=eZines))_.

This is an official repository of [The Exploit Database](https://www.exploit-db.com/), a [project](https://www.offensive-security.com/community-projects/) sponsored by [Offensive Security](https://www.offensive-security.com/).
Our repositories are:

  - Exploits & Shellcodes: [https://gitlab.com/exploit-database/exploitdb](https://gitlab.com/exploit-database/exploitdb)
  - Binary Exploits: [https://gitlab.com/exploit-database/exploitdb-bin-sploits](https://gitlab.com/exploit-database/exploitdb-bin-sploits)
  - Papers: [https://gitlab.com/exploit-database/exploitdb-papers](https://gitlab.com/exploit-database/exploitdb-papers)

The Exploit Database is an archive of public exploits and corresponding vulnerable software, developed for use by penetration testers and vulnerability researchers. Its aim is to serve as the most comprehensive collection of [exploits](https://www.exploit-db.com/), [shellcode](https://www.exploit-db.com/shellcodes) and [papers](https://www.exploit-db.com/papers) gathered through direct submissions, mailing lists, and other public sources, and present them in a freely-available and easy-to-navigate database. The Exploit Database is a repository for exploits and Proof-of-Concepts rather than advisories, making it a valuable resource for those who need actionable data right away.
You can learn more about the project [here (Top Right -> About Exploit-DB)](https://www.exploit-db.com/) and [here (History)](https://www.exploit-db.com/history).

This repository is updated daily with the most recently added submissions.

- - -

## License

This project is released under "[GNU General Public License v2.0](https://gitlab.com/exploit-database/exploitdb-papers/-/blob/main/LICENSE.md)".

- - -

# SearchSploit

**Kali Linux**

A simple `apt install` will do the trick _(everything will be taken care of)_:

```
kali@kali:~$ sudo apt -y install exploitdb exploitdb-papers
```

There isn't any need to use the `git` section below, if you are a Kali user _(unless you want the latest papers)_.

- - -

**Git**

```
$ sudo git clone https://gitlab.com/exploit-database/exploitdb.git /opt/exploitdb
$ sudo git clone https://gitlab.com/exploit-database/exploitdb-papers.git /opt/exploitdb-papers
$ sudo ln -sf /opt/exploitdb/searchsploit /usr/local/bin/searchsploit
$ cp -n /opt/exploitdb/.searchsploit_rc ~/
$ vim ~/.searchsploit_rc
```

If you wish for this to be integrated into [SearchSploit](https://www.exploit-db.com/searchsploit) allowing for quick offline searching, you will need to make sure your `.searchsploit_rc` information is accurate for your setup (mainly `path_array`):

```
$ cat ~/.searchsploit_rc
[...SNIP...]
##-- Papers
files_array+=("files_papers.csv")
path_array+=("/opt/exploitdb-papers")
name_array+=("Paper")
git_array+=("https://gitlab.com/exploit-database/exploitdb-papers.git")
package_array+=("exploitdb-papers")
$
```

_Make sure the `##-- Papers` section is correct for you_
