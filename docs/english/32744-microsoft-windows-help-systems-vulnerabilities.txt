1)      Introduction


Microsoft Windows operating systems, until Windows 8 have had in total four help systems.  The first one released in the times of Windows 3x is called “WinHelp” and contains a file association named “HLP”.  Basicly is a set of RTF documents with some special formatting, bitmap images all compiled in a binary file (.HLP) and ability to run Macros automatically. This includes, jump help contents, display “open file” dialog, execute programs with parameters , open files and run DLL functions. There exists the 16 and 32-bits Winhelp. 16-bits “.HLP” files should be opened in “winhelp.exe” application and 32-bits “.HLP” files should be opened in “Winhlp32.exe” application. Native support for this Help System ended on Windows XP and 2003. On Windows Vista and above only 16-bits “.HLP” files can be opened, in “Winhelp.exe” application. That is, if the operating system architecture is 32-bits.

The second one released with Internet Explorer 4,  and built into Windows 98, called “HTML Help” is a set of HTML documents, a table of contents file (optional) and an Index file (optional) all compiled in a binary file with the CHM extension. It comes with an ActiveX control, that can be embedded in Internet Explorer.  It has the ability to run programs with parameters via the “Shortcut” parameter of the ActiveX, open arbitrary WinHelp (.HLP) files from the local computer or SMB and WebDAV shares, via the “winhelp” parameter of the ActiveX and inject script code in arbitrary pages when the user clicks a Table of contents topic that has a “Javascript:” URL protocol. This is called cross domain scripting.

The third one, built into Windows XP and 2003 is the “Help and Support  Center”.  It is basicly a combination between compiled HTML files (.CHM) , standalone HTML files and special XML files to use in the Window definition. Standalone HTML files are called by its internal URL protocol, called “HCP:”. This URL protocol is also registered for the “Help and Support Center” (helpctr.exe). Files invoked by this URL protocol are treated like HTML applications and have the ability to initialize and script arbitrary ActiveX controls.  It has a nicer look compared with the classic HTML Help.

The fourth one is the official new Help system after the HTML Help and started shipping with Windows Vista. It is also HTML based, but the HTML files are compiled into a binary (.H1S) file using the Windows PE file and they are invoked by the “MS-HELP:” *internal* URL protocol. They are displayed in the “Help Pane” (helppane.exe) window. There is no file or URL protocol registered for this Help system. It is used by Windows native applications and third parties vendors local files.

Because these types of files and the “HCP:” URL protocol are, by design, able to run arbitrary code on a system, anything that bypasses security mechanisms of the operating system, involving them is considered a vulnerability. Naturally, the “.HLP” and “.CHM” extensions are in the blacklist of files of Microsoft.



2)      Vulnerabilities involving these help systems.

  A)     Vulnerabilities involving Winhelp:

    1)      Microsoft VBScript  “MsgBox()” arbitrary HLP file execution vulnerability.

  This vulnerability could be exploited remotely via Internet Explorer, by pressing the “F1” key when a Message Box appeared and pointed to a help file located in a WebDAV or SMB share.  Reference: http://www.securityfocus.com/bid/38463



    2)      Microsoft “Winhlp32” HLP file processing stack overflow vulnerability.  The vector for this vulnerability was the above vulnerability, but it is kind of pointless since “.HLP” files can run arbitrary code by design, via Macros.

Reference:  http://www.securityfocus.com/bid/38473



    3)      Multiple programs “HLP” file loading hijack vulnerability.

    This vulnerability can be exploited remotely via SMB and WebDAV shares. Every program or dialog that has a question mark “?” , when the “F1” key is pressed, will run Winhlp32.exe passing a relative path parameter for a help file with specific file name and help sensitive context ID (numerical value).  By opening a file in a WebDAV or SMB share and then pressing “F1” or a combination of keys like “CTRL+O+F1” will cause a help file to be searched in the share. If it exists it will be loaded.  The context sensitive ID must be known, for this, the legitimate help file located at “%systemroot%\help” must be decompiled, using a tool such as “HelpDeco”. Programs includes Notepad, Internet Explorer, Video display properties (via .THEME files), Windows “Open file with” dialog, Mozilla Firefox, Google Chrome, etc…

    Reference:

    http://secumania.info/joomla/index.php/component/k2/item/15-multiple-programs-remote-hlp-winhelp-file-load-hijacking-vulnerability


  B)      Vulnerabilities in HTML Help

  Since HTML Help contains an ActiveX that can be embedded in IE, this has long been targeted by attackers.  It has had buffer overflows vulnerabilities, script  injection in local

  “.CHM” help files and further abusing the “shortcut” parameter of the ActiveX, local file access vulnerabilities. And finally on 2004/2005 it has been used to bypass an important security feature recently introduced in Internet Explorer 6 (on Windows XP SP2) called

  “Local machine zone lockdown”.  This feature causes  Internet Explorer to change its security setting for the local machine zone so that ActiveX controls cannot be initialized and script code cannot be run. On march 2010 I have published a vulnerability I found about an year back which is a CHM help file loading hijack, and used TXT documents as the main attack vector. This vulnerability also caused the classic “open file” security warning to be bypassed, when the user opened the TXT document from a WebDAV or SMB share and pressed the “F1” key.  On 2011, a stack overflow vulnerability has been found and could be combined with the CHM help file loading hijack vulnerability, reducing the amount of user interaction.



  References:

  http://secunia.com/advisories/38916

  http://aluigi.altervista.org/adv/chm_1-adv.txt





  C)      Vulnerabilities in Microsoft Help and Support Center

    1)      Help and Support Center DVD upgrade “website” parameter  arbitrary URL injection vulnerability (MS04-015).  This vulnerability allowed an attacker  to inject an “HTTP://” URL in Help and Support Center when the user pressed the “upgrade now” button :  HCP://system/DVDUpgrd/dvdupgrd.htm?website=somesite.com/

    Reference:  http://exploitlabs.com/files/advisories/EXPL-A-2003-027-helpctr.txt


    2)      Help and Support Center File deletion vulnerability. This vulnerability allowed attackers to delete arbitrary files on a user´s system :

    Hcp://system/dfs/uplddrvinfo.htm?c:\somefile

    Reference:  http://www.securityfocus.com/bid/5478/exploit



    3)      Help and Support Center URI Handler Buffer Overflow vulnerability

    Buffer overflow in a specially crafted HCP:// URL

    Reference: http://www.securityfocus.com/bid/8828/discuss



    4)      Help and Support Center Command Line Injection vulnerability.

    This vulnerability allowed an attacker to inject arbitrary URLs automatically in Help and Support Center:



    Hcp:// “  -url “ http://somesite.com



    5)      Help and Support Center Whitelist bypass vulnerability

    Since the release of Service Pack 1 for Windows XP, Help and Suport Center did not allow passing of parameters to internal  files anymore and just some of them were available when called by the “HCP://” URL protocol. This vulnerability allowed an attacker to bypass the whitelist, and invoke arbitrary internal files. This could also allow injection of arbitrary external URLs like an HTTP:// site if the URL



    hcp://system/errors/connection.htm?online_url=http://site.com



    was invoked. Combined with a cross domain vulnerability in IE, an attacker could run arbitrary code in the context of the local machine zone since it is allowed to access local files.



    6)      Help and Support Center Cross Site Scripting Weakness.

    A weakness exists in Help and Support Center that allowed an attacker to inject HTML and script code in an internal file (sysinfomain.htm). Since it is processed by the “HCP://” URL protocol, it can run arbitrary script code:



    hcp://system/sysinfo/sysinfomain.htm?svr=<script>a=new%20ActiveXObject(‘Wscript.Shell’);a.Run(‘calc.exe’);</script>



    7)      Help and Support Center “topic” parameter cross domain scripting vulnerability

    This vulnerability I found myself on XP SP0 and never seen it anywhere, so it was possibly silently corrected in some update.  This vulnerability would have allowed an attacker to inject script code in arbitrary domains or security zones leading to arbitrary code execution :



    Hcp://system/centers/support?topic=javascript:alert(location.href);



    Above code would inject script code in hcp://system/firstpage.htm



    But an attacker could inject code in any page :



    Hcp://system/centers/support?topic=http://www.google.com



    Then after some seconds:



              Hcp://system/centers/support?topic=javascript:alert(location.href);

    Script code would be injected in google.com .



    8)      Help and Support Center of Windows ME “REALURL” parameter arbitrary script execution vulnerability.

    Another vulnerability I found out and never seen it published that allowed attackers to run arbitrary script code on the target computer, upon clicking a link. This could be combined with a click hijack vulnerability, or something similar because the user would need to click that link to trigger the vulnerability:



    Hcp://system/error.htm?REALURL=javascript:eval(alert(location.href))



    9)      Help And Support Center Drag and Drop operation URL injection weakness.

    A weakness I found in CHM help topics displayed by Help and Support Center that allows dropping links and files inside it. Upon dropping an HTTP://  URL it will be processed by Help and Support Center. This can be combined with the XSS weakness to run arbitrary code in the system.



    Reference:

    http://secumania.info/joomla/index.php/component/k2/item/16-microsoft-windows-help-and-support-center-multiple-vulnerabilities






  D)     Vulnerabilities in Microsoft Help 2.0 (Help Pane)

  No vulnerabilities up to the date of this writing because there is no attack vector. The only possible theoretical attack vector is a DNS poisoning attack which could direct the domain name of Microsoft to another IP of the attacker´s choice when using the online help of Help Pane, but the minimum amount of user interaction would be pressing the “F1” key on some application that has also online topics.



Author: Eduardo Prado.

Vendor: Microsoft Corporation

Date: April 7th, 2014
