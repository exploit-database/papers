##########################################
Hacking Android Apps for Fun and Profit
##########################################
#Author: G13
#Twitter: @g13net
#Email: g13net@gmail.com
##########################################


##### 0x0 ToC #####

0x1 Intro
0x2 Dalvik Primer
0x3 Case Studies
0x4 Additional Notes
0x5 Resources

##### 0x1 Intro #####

Android is a mobile OS owned by Google.  Android allows developers to write applications("apps") for the OS and distribute them through
the Google Play Store.  These apps can be free or need to be purchased.  Free apps typically have ads in them to give the developer additional
revenue.  This paper will dive into patching disassembled Android apps for our benefit.

##### 0x2 Dalvik Primer #####

Android apps are generally written in Java.  When the app is compiled, the Java byte-code is converted into Dalvik bytecode(.dex files).  This conversion allows the apps to be run in the Dalvik VM environment that is used by Android.

Once an app is disassembled, we are presented with Dalvik Opcodes, see the below example

## Code Snip ##

    iput-object p3, p0, Lb;->a:Ljava/io/Writer;

    .line 44
    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lb;->b:Z

    .line 46
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_1

## End Snip ##

The if-xxx opcodes are conditional opcodes.  The :cond_1 specifies the jump point in the code when the condition is matched.  'move' moves the value of one register to another.  For more details on opcode references, see section 0x5 References for a link.

##### 0x3 Case Studies #####

#### 0x3a Coloring Book for Kids ####

App Name: Coloring Book for Kids
Goal: Remove Ads

For this app, we don't need to dive into Dalvik code.  We just have to inspect the contents of the layout files.  Once the app is disassembled, look in the
Res/layout/main.xml file.  This XML file describes where different widgets will be placed on the screen.  After review of the file we will come across
this section:

## Code Snip ##

<RelativeLayout android:orientation="vertical" android:id="@id/colorsLayout" android:layout_width="fill_parent" android:layout_height="fill_parent"
      xmlns:android="http://schemas.android.com/apk/res/android"
      xmlns:ads="http://schemas.android.com/apk/lib/com.google.ads">
        <GridView android:gravity="center" android:id="@id/colorView" android:layout_width="fill_parent" android:layout_height="fill_parent" android:horizontalSpacing="15.0dip" android:verticalSpacing="0.0dip" android:stretchMode="columnWidth" android:columnWidth="30.0dip" android:numColumns="auto_fit" android:layout_above="@id/colorsAdMob"
          xmlns:android="http://schemas.android.com/apk/res/android" />
        <com.google.ads.AdView android:id="@id/colorsAdMob" android:layout_width="fill_parent" android:layout_height="fill_parent" android:layout_alignParentBottom="true" ads:adUnitId="a14d5ae1ff5b91c" ads:adSize="BANNER" ads:testDevices="TEST_EMULATOR, TEST_DEVICE_ID" ads:loadAdOnCreate="true" />
    </RelativeLayout>

## End Snip ##

If we change the android:layout_width and android:layout_height attributes to be "0px" the ad will not be viewable on the screen.  The only downside to this approach is that the ad code will still run; so the app will still send your information off to the provider for statistics.  The changed code will look like this:

## Code Snip ##

<RelativeLayout android:orientation="vertical" android:id="@id/colorsLayout" android:layout_width="fill_parent" android:layout_height="fill_parent"
      xmlns:android="http://schemas.android.com/apk/res/android"
      xmlns:ads="http://schemas.android.com/apk/lib/com.google.ads">
        <GridView android:gravity="center" android:id="@id/colorView" android:layout_width="0px" android:layout_height="0px" android:horizontalSpacing="15.0dip" android:verticalSpacing="0.0dip" android:stretchMode="columnWidth" android:columnWidth="30.0dip" android:numColumns="auto_fit" android:layout_above="@id/colorsAdMob"
          xmlns:android="http://schemas.android.com/apk/res/android" />
        <com.google.ads.AdView android:id="@id/colorsAdMob" android:layout_width="0px" android:layout_height="0px" android:layout_alignParentBottom="true" ads:adUnitId="a14d5ae1ff5b91c" ads:adSize="BANNER" ads:testDevices="TEST_EMULATOR, TEST_DEVICE_ID" ads:loadAdOnCreate="true" />
    </RelativeLayout>

## End Snip ##

#### 0x3b Solitaire ####

App Name: Solitaire by Mobilityware
Goal: Remove Ads

To remove the ads from this app, we will have to modify some Dalvik code.  Whenever a new round is dealt, an ad screen will pop up to the user.  The user then has to  "dismiss" the ad before they are returned to the game.

I first started greping through the smali files looking for common keywords: displayad, viewad, getad.  I came across the following line in the com/mobilityware/solitaire/Solitaire.smali file:

## Code Snip ##

02204: invoke-virtual {v0}, Lcom/mobilityware/solitaire/AdControl;->displayAd()Z

## End Snip ##

The 'invoke-virtual' opcode calls a virtual method.  In this case it is calling the displayAd function in com/mobilityware/solitaire/AdControl.  If we comment out this call, the ads will not be displayed:

## Code Snip ##

02204: #invoke-virtual {v0}, Lcom/mobilityware/solitaire/AdControl;->displayAd()Z

## Code Snip ##

#### 0x3c Chess Free ####

App Name: Chess Free by aifactory
Goal: Remove Ads

The ads in Chess are displayed while a user is playing the game.  Chess Free uses a different ad engine than the previous apps.  For this app, I decided to take a different approach: prevent the ad system from receiving ads.

After running logcat on the phone, noticed that there were calls to "adRequestWebView" being made.  After greping through the files, in google/ads/c.smali I found the following lines of code:

## Code Snip ##

01    :try_start_0
02   iget-object v0, p0, Lcom/google/ads/c;->f:Landroid/webkit/WebView;
03
04    if-eqz v0, :cond_0
05
06    iget-object v0, p0, Lcom/google/ads/c;->c:Lcom/google/ads/b;
07
08    if-nez v0, :cond_1
09
10    :cond_0
11    const-string v0, "adRequestWebView was null while trying to load an ad."
12
13    invoke-static {v0}, Lcom/google/ads/util/a;->e(Ljava/lang/String;)V
14
15    sget-object v0, Lcom/google/ads/AdRequest$ErrorCode;->INTERNAL_ERROR:Lcom/google/ads/AdRequest$ErrorCode

## End Snip ##

In the above code, there is a test on v0 to see if it is zero and if it is to jump to the :cond_0 statement.  If :cond_0 is hit, the function throws an error that the ad could not load; this seems like a great place to introduce some of our own logic!

If we can set the value of v0 to be '0' before it hits the condition in line 04, the cond_0 section will be hit.  We can introduce this value by using the 'const' statement.  We will introduce "const v0, 0x0" before the "if-eqz v0, :cond_0" statement to ensure that cond_0 will be hit.  See in the below code:

## Code Snip ##

01    :try_start_0
02    iget-object v0, p0, Lcom/google/ads/c;->f:Landroid/webkit/WebView;
03
04    const v0, 0x0
05
06    if-eqz v0, :cond_0
07
08    iget-object v0, p0, Lcom/google/ads/c;->c:Lcom/google/ads/b;
09
10    if-nez v0, :cond_1
11
12    :cond_0
13    const-string v0, "adRequestWebView was null while trying to load an ad."

## End Snip ##

Now with the value introduced, the ads will not load during the game.

##### 0x4 Additional Notes #####

This paper did not discuss how to disassemble an Android application and reassemble it after the changes have been made.  There are numerous resources available that discuss how to reverse engineer Android applications.  In the Resources section I have included a link to a tool that has made the job way easier.

##### 0x5 Resources #####

http://pallergabor.uw.hu/androidblog/dalvik_opcodes.html
http://www.virtuousrom.com/p/ten-studio.html