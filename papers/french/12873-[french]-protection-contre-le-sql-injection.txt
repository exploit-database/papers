/*

              _____       _ ___        __
             | ____|_   _(_) \ \      / /_ _ _   _
             |  _| \ \ / / | |\ \ /\ / / _` | | | |
             | |___ \ V /| | | \ V  V / (_| | |_| |
             |_____| \_/ |_|_|  \_/\_/ \__,_|\__, |
                                              |___/
                                    _____
                                   |_   _|__  __ _ _ __ ___
                                      | |/ _ \/ _` | '_ ` _ \
                                      | |  __/ (_| | | | | | |
                                      |_|\___|\__,_|_| |_| |_|

[french] Protection contre le SQL Injection

Tutorial par : Moudi
Contact : <m0udi@9.cn>
English ( Avoiding SQL Injection ) : http://milw0rm.com/papers/358

Greetings : Mizoz, Zuka, str0ke, 599eme Man.
Please visit: http://unkn0wn.ws/board/index.php

*/

--------------------------------------------------------------------

Les SQL Injection sont parmi les failles les plus rÃ©pandus et dangereux en PHP.
Ce tutorial va expliquer clairement le concept de SQL Injection et comment les Ã©viter, une fois pour toutes.

>>>>>> >>>>>>
RÃ©sumÃ© de tutorial:
   I) PrÃ©sentation du problÃ¨me.
      => Les variables contenant des chaÃ®nes
   Ii) la sÃ©curitÃ©.
      => Explication
      => Les variables numÃ©riques
         . MÃ©thode 1
         . MÃ©thode 2
>>>>>> >>>>>>

--------------------------------------------------------------------


I) PrÃ©sentation du problÃ¨me.
    ___________________________

Il existe deux types d'injection SQL:

* L'injection dans les variables qui contiennent des chaÃ®nes;
* L'injection dans les variables numÃ©riques.

Ce sont deux types trÃ¨s diffÃ©rents et a Ã©viter, il agira
diffÃ©remment pour chacun de ces types.

######################
Les variables qui contiennent des chaÃ®nes:
######################

Imaginez un script PHP qui rÃ©cupÃ¨re l'Ã¢ge d'un membre conformÃ©ment Ã  son
surnom. Ce surnom est parti d'une page Ã  l'autre via l'URL
(en $ _GET quoi: p). Ce script devrait ressembler Ã  ceci:

++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++
...
$ pseudo = $ _GET [ 'pseudo'];
$ requete = mysql_query ( "SELECT age FROM membres WHERE pseudo = '$ pseudo'");
...
++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++


Eh bien tenez vous bien, ce script est une grande vulnÃ©rabilitÃ© par injection SQL.
Il suffit d'un mauvais garÃ§on en mettant en place le nom d'utilisateur dans l'URL d'une requÃªte
comme ceci:

++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++
'Password UNION SELECT FROM membres WHERE id = 1
++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++


Il est d'arriver Ã  montrer (juste un exemple), par exemple le mot de passe
le membre ayant l'ID 1. Je ne vais pas expliquer en dÃ©tail le fonctionnement de
la peur que quelqu'un n'est pas agrÃ©able pour s'y promener. Eh bien, laissez-nous donc aller Ã  la
la sÃ©curitÃ©:).

Ii) la sÃ©curitÃ©.
_______________

Pour sÃ©curiser ce type d'injection est simple. Vous utilisez la fonction
mysql_real_escape_string ().

######################
Euh ... Elle fait ce que c'est?
######################

Cette fonction ajoute le caractÃ¨re "\" pour les caractÃ¨res suivants:

++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++
NULL, \ x00, \ n, \ r, \, ', "et \ X1A
++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++

######################
And what's the point?
######################

Comme vous l'avez remarquÃ© dans l'injection prÃ©cÃ©dente, l'attaquant utilise la citation
(pour fermer la 'Around $ nick): si elle est empÃªchÃ©e de le faire, l'
bad boy n'auront qu'Ã  aller voir ailleurs. Cela signifie que si l'on
applique un mysql_real_escape_string () pour le nom de variable comme Ã§a ...

++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++
...
$ pseudo = mysql_real_escape_string ($ _GET [ 'pseudo']);
$ requete = mysql_query ( "SELECT age FROM membres WHERE pseudo = '$ pseudo'");
...
++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++

L'application est entiÃ¨rement sÃ©curisÃ©.
Explication

######################
Hacker par injection d'un rappel:
######################

++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++
'Password UNION SELECT FROM membres WHERE id = 1
++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++

Eh bien, si nous appliquons mysql_real_escape_string () Ã  la variable $ nom utilisÃ©
dans la requÃªte est ce qui va de l'injection:

++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++
\ 'Union de passe SÃ©lectionnez membres WHERE id = 1
++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++

Cela signifie que nous ne viennent pas mÃªme en dehors des quotes autour de $ nick dans
la requÃªte car la \ a Ã©tÃ© ajoutÃ©e. Il existe une autre fonction
quelque peu semblable Ã  mysql_real_escape_string () est addslashes (), pourquoi
ne pas avoir utilisÃ©? Eh bien rÃ©cemment, une faille de sÃ©curitÃ© a Ã©tÃ© dÃ©couvert Ã  ce sujet si
Il est utilisÃ© sur une installation de PHP 4.3.9 avec magic_quotes_gpc est activÃ©.

######################
Variables numÃ©riques:
######################

Ce type d'injection est moins connue que la prÃ©cÃ©dente, ce qui en fait
plus frÃ©quentes, et il commence comme tout Ã  l'heure avec un exemple. Cette fois, c'est
affichage de l'Ã¢ge d'un membre en fonction de son id, et en le faisant passer par un
formulaire ($ _POST) Ã  changer:

++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++
...
$ id = $ _POST [ 'id'];
$ requete = mysql_query ( "SELECT age FROM membres WHERE id = $ id");
...
++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++


mysql_real_escape_string () serait rien ici, car si un attaquant
veut injecter du code SQL, il n'aura pas besoin d'utiliser des guillemets, parce que le
variable $ id n'est pas entourÃ© de guillemets. Simple exemple d'une
exploitation:

++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++
2 UNION DE passe SÃ©lectionnez membres WHERE id = 1
++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++

Cette injection a fait exactement la mÃªme que la prÃ©cÃ©dente, sauf que
ici, pour l'Ã©viter, il ya deux solutions:

      * Changer le contenu de la variable si elle contient uniquement des chiffres;
      * VÃ©rifiez si la variable contient en rÃ©alitÃ© un certain nombre avant de l'utiliser dans une requÃªte.

##########
MÃ©thode 1:
##########

Nous allons utiliser une fonction intval () Cette fonction retourne quel que soit le
contenu d'une variable sa valeur numÃ©rique. Par exemple:

++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++
e10 variable = '1 '; / / $ variable vaut '1 e10'
valeur_numerique $ = intval ($ variable); / / $ vaut valeur_numerique 1
++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++

Maintenant, revenons Ã  nos moutons:

++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++
$ id = intval ($ _POST [ 'id']);
$ requete = mysql_query ( "SELECT age FROM membres WHERE id = $ id");
)
++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++

Cela signifie que vous pouvez y passer et est plus que suffisante, mais je vous recommande
continuer Ã  trouver une autre mÃ©thode, ou si vous avez l'air bÃªte si vous trouvez cette
mÃ©thode sur un code qui n'est pas le vÃ´tre sans le comprendre.

############
MÃ©thode 2:
###########

Ici, nous utilisons une fonction qui renvoie TRUE si une variable ne contient que
nombres et FALSE si elle n'est pas le cas cette fonction is_numeric (),
nous allons l'utiliser dans une condition qui vÃ©rifie si la fonction is_numeric () retourne
TRUE bien.

++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++
$ id = $ _POST [ 'id'];
if (is_numeric ($ id))
(
$ requete = mysql_query ( "SELECT age FROM membres WHERE id = $ id");
)
autre
(
echo "Essayer de me hack? MOTHA FUCKAH xD";
++++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++++++++++++++++++++++++++++++++++++++

################################################## ################
Quel est le meilleur, en fonction intval () et is_numeric ()?
################################################## ################

Eh bien je dirai que les deux sont tout aussi efficaces, ils sont Ã©gaux.
Mais je prÃ©fÃ¨re inval () car avec la fonction is_numeric () Ã©crire plus de code, et si
la variable ne contient pas seulement des chiffres, la demande est annulÃ©e (en
principe, mais bien sÃ»r vous pouvez exÃ©cuter la mÃªme requÃªte en choisissant une
Valeur par dÃ©faut de la variable utilisÃ©e). Well that's it! Vous savez tout
sÃ©curiser vos applications. Si vous appliquez ces mÃ©thodes, il ya
absolument aucun risque d'avoir un type de faille d'injection SQL sur son site web
(ou PHP).

# milw0rm.com [2009-09-03]