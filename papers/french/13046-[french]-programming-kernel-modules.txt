Avant-Propos
1. Mode noyau et module
1.1. DÃ©bogage en mode noyau
1.2. Chargement et dÃ©chargement de module
1.3. Description de module
1.4. Passage de paramÃ¨tres
2. Driver en mode caractÃ¨re
2.1. Ajout d'un driver au noyau
2.2. ImplÃ©mentation des appels systÃ¨mes
2.3. MÃ©thodes open et release
2.4. Allocation mÃ©moire
2.5. MÃ©thodes read et write
2.6. MÃ©thode ioctl
2.7. Gestion d'interruptions
=========================================================================================
1. Mode noyau et module

1.1. DÃ©bogage en mode noyau
Le dÃ©bogage en mode noyau peut se faire via plusieurs outils.

    * En mode console, via printk() et dmesg pour voir les messages;
    * Avec kgdb, nÃ©cessite une seconde machine reliÃ©e par cÃ¢ble sÃ©rie et possÃ©dant GDB-client pour dÃ©boguer la cible;
    * Avec kdb, un dÃ©bogeur noyau embarquÃ©.

Il existe d'autres mÃ©thodes mais, ne les ayant jamais utilisÃ©es, je n'en parlerai pas.

Prototype :

int printk(const char *fmt, ...)

Exemple :

printk("<1> Hello World !\n");

Plusieurs niveaux de dÃ©bogage sont dÃ©finis dans <linux/kernel.h>


#define KERN_EMERG    "<0>"  /* systÃ¨me inutilisable */
#define KERN_ALERT    "<1>"  /* action Ã  effectuer immÃ©diatement*/
#define KERN_CRIT     "<2>"  /* conditions critiques */
#define KERN_ERR      "<3>"  /* conditions d'erreurs */
#define KERN_WARNING  "<4>"  /* message d'avertissement */
#define KERN_NOTICE   "<5>"  /* normal mais significatif */
#define KERN_INFO     "<6>"  /* informations */
#define KERN_DEBUG    "<7>"  /* messages de dÃ©bugging */

Du coup cela devient :

printk(KERN_ALERT "Hello World !\n");

La commande dmesg permet d'afficher les messages de printk()


1.2. Chargement et dÃ©chargement de module
Un module possÃ¨de un point d'entrÃ©e et un point de sortie.

    * point d'entrÃ©e : int xxx(void)
    * point de sortie : void yyy(void)

OÃ¹ xxx et yyy sont ce que vous voulez. Pour dire au module que ces 2 fonctions sont le point d'entrÃ©e et le point de sortie, nous utilisons ces 2 macros.

    * module_init(xxx);
    * module_exit(yyy)

Dans la suite de l'article les points d'entrÃ©e et de sortie sont nommÃ©s module_init() et module_exit().

Ces 2 fonctions sont automatiquement appelÃ©es, lors du chargement et du dÃ©chargement du module, avec insmod et rmmod.

Remarque : Un module ne possÃ¨de pas de fonction main().

La fonction module_init() doit s'occuper de prÃ©parer le terrain pour l'utilisation de notre module (allocation mÃ©moire, initialisation matÃ©rielle...).

La fonction module_exit() doit quant Ã  elle dÃ©faire ce qui a Ã©tÃ© fait par la fonction module_init().

Voici le source minimum d'un module


#include <linux/module.h>
#include <linux/init.h>

static int __init mon_module_init(void)
{
	printk(KERN_DEBUG "Hello World !\n");
	return 0;
}

static void __exit mon_module_cleanup(void)
{
    printk(KERN_DEBUG "Goodbye World!\n");
}

module_init(mon_module_init);
module_exit(mon_module_cleanup);

Vous pouvez ainsi appeler vos init et cleanup comme bon vous semble.

Pour compiler c'est trÃ¨s simple, il faut utiliser cette commande :
# make

Avec le makefile suivant :


obj-m += module.o

default:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

Chargement et dÃ©chargement :
$ insmod ./module.ko
$ lsmod
$ rmmod module.ko
$ dmesg


1.3. Description de module
Vous pouvez dÃ©crire vos modules Ã  l'aide des diffÃ©rentes macros mises Ã  votre disposition dans <linux/module.h>.

MODULE_AUTHOR(nom) : place le nom de l'auteur dans le fichier objet
MODULE_DESCRIPTION(desc) : place une description du module dans le fichier objet
MODULE_SUPPORTED_DEVICE(dev) : place une entrÃ©e indiquant le pÃ©riphÃ©rique pris en charge par le module.
MODULE_LICENSE(type) : indique le type de licence du module

On peut obtenir ces informations avec la commande modinfo nom_module.


#define MODULE
#include <linux/module.h>
#include <linux/init.h>

MODULE_AUTHOR("djekmani");
MODULE_DESCRIPTION("exemple de module");
MODULE_SUPPORTED_DEVICE("none");
MODULE_LICENSE("none");


static int __init mon_module_init(void)
{
    printk(KERN_DEBUG "Hello World !\n");
    return 0;
}

static void __exit mon_module_cleanup(void)
{
    printk(KERN_DEBUG "Goodbye World!\n");
}

module_init(mon_module_init);
module_exit(mon_module_cleanup);


1.4. Passage de paramÃ¨tres
Comme vous vous en doutez, il serait bien utile de passer des paramÃ¨tres Ã  notre module.
une fonction et une macro sont donc disponibles pour cela

module_param(nom, type, permissions)
MODULE_PARM_DESC(nom, desc)

Plusieurs types de paramÃ¨tres sont actuellement supportÃ©s pour le paramÃ¨tre type : short (entier court, 2 octet), int (entier, 4 octets), long (entier long) et charp (chaÃ®nes de caractÃ¨res).

Dans le cas de tableau, vous devez utiliser la fonction : module_param_array(name, type, addr, permission);
addr est l'adresse d'une variable qui contiendra le nombre d'Ã©lÃ©ment initialisÃ©s par la fonction.


#include <linux/module.h>
#include <linux/init.h>

MODULE_AUTHOR("djekmani4ever");
MODULE_DESCRIPTION("exemple de module");
MODULE_SUPPORTED_DEVICE("none");
MODULE_LICENSE("none");

static int param;

module_param(param, int, 0);
MODULE_PARM_DESC(param, "Un paramÃ¨tre de ce module");

static int __init mon_module_init(void)
{
    printk(KERN_DEBUG "Hello World !\n");
    printk(KERN_DEBUG "param=%d !\n", param);
    return 0;
}

static void __exit mon_module_cleanup(void)
{
    printk(KERN_DEBUG "Goodbye World!\n");
}

module_init(mon_module_init);
module_exit(mon_module_cleanup);

Test:
$insmod ./module.o param=2

VoilÃ , maintenant que vous connaissez les bases d'un module, nous allons voir la crÃ©ation d'un driver et les interactions possibles avec le mode utilisateur, et le matÃ©riel.


2. Driver en mode caractÃ¨re
Un driver en mode caractÃ¨re permet de dialoguer avec le pÃ©riphÃ©rique, en Ã©changeant des informations. Il existe d'autre driver, dit en mode bloc, qui Ã©changent des donnÃ©es uniquement par bloc de donnÃ©es (disque dur par exemple).
Dans cet article, nous nous intÃ©resserons uniquement aux drivers en mode caractÃ¨re.


2.1. Ajout d'un driver au noyau
Lors de l'ajout d'un driver au noyau, le systÃ¨me lui affecte un nombre majeur. Ce nombre majeur a pour but d'identifier notre driver.

L'enregistrement du driver dans le noyau doit se faire lors de l'initialisation du driver (c'est Ã  dire lors du chargement du module, donc dans la fonction init_module()), en appelant la fonction : register_chrdev().

De mÃªme, on supprimera le driver du noyau (lors du dÃ©chargement du module dans la fonction cleanup_module()), en appelant la fonction : unregister_chrdev().

Ces fonctions sont dÃ©finies dans <linux/fs.h>

Prototypes :


int register_chrdev(unsigned char major, const char *name, struct file_operations *fops);
int unregister_chrdev(unsigned int major, const char *name);

Ces fonctions renvoient 0 ou >0 si tout se passe bien.

register_chrdev

    * major : numÃ©ro majeur du driver, 0 indique que l'on souhaite une affectation dynamique.
    * name : nom du pÃ©riphÃ©rique qui apparaÃ®tra dans /proc/devices
    * fops : pointeur vers une structure qui contient des pointeurs de fonction. Ils dÃ©finissent les fonctions appelÃ©es lors des appels systÃ¨mes (open, read...) du cÃ´tÃ© utilisateur.

unregister_chrdev

    * major : numÃ©ro majeur du driver, le mÃªme qu'utilisÃ© dans register_chrdev
    * name : nom du pÃ©riphÃ©rique utilisÃ© dans register_chrdev


static struct file_operations fops =
{
    read : my_read_function,
    write : my_write_function,
    open : my_open_function,
    release : my_release_function /* correspond a close */
};

Une autre faÃ§on de dÃ©clarer la structure file_operations, qui reste plus portable et "correcte" est la suivante


struct file_operations fops =
{
    .read = my_read_function,
    .write = my_write_function,
    .open = my_open_function,
    .release = my_release_function  /* correspond a close */
};

Certaines mÃ©thodes non implÃ©mentÃ©es sont remplacÃ©es par des mÃ©thodes par dÃ©faut. Les autres mÃ©thodes non implÃ©mentÃ©es retournent -EINVAL.


2.2. ImplÃ©mentation des appels systÃ¨mes


static ssize_t my_read_function(struct file *file, char *buf, size_t count, loff_t *ppos)
{
    printk(KERN_DEBUG "read()\n");
    return 0;
}

static ssize_t my_write_function(struct file *file, const char *buf, size_t count, loff_t *ppos)
{
    printk(KERN_DEBUG "write()\n");
    return 0;
}

static int my_open_function(struct inode *inode, struct file *file)
{
    printk(KERN_DEBUG "open()\n");
    return 0;
}

static int my_release_function(struct inode *inode, struct file *file)
{
    printk(KERN_DEBUG "close()\n");
    return 0;
}

Ces fonctions renvoient 0 (ou >0) en cas de succÃ¨s, une valeur nÃ©gative sinon.

La structure file dÃ©finie dans <linux/fs.h> reprÃ©sente un fichier ouvert et est crÃ©ee par le noyau sur l'appel systÃ¨me open(). Elle est transmise Ã  toutes fonctions qui agissent sur le fichier, jusqu'Ã  l'appel de close().

Les champs les plus importants sont :

    * mode_t f_mode : indique le mode d'ouverture du fichier
    * loff_t f_pos : position actuelle de lecture ou d'Ã©criture
    * unsigned int f_flags : flags de fichiers (O_NONBLOCK...)
    * struct file_operations *f_op : opÃ©rations associÃ©es au fichier
    * void *private_data : le driver peut utiliser ce champ comme il le souhaite

Un fichier disque sera lui reprÃ©sentÃ© par la structure inode. On n'utilisera gÃ©nÃ©ralement qu'un seul champ de cette structure :
kdev_t i_rdev : le numÃ©ro du pÃ©riphÃ©rique actif

Ces macros nous permettrons d'extraire les nombres majeurs et mineurs d'un numÃ©ro de pÃ©riphÃ©rique :
int minor = MINOR(inode->i_rdev);
int major = MAKOR(inode->i_rdev);


2.3. MÃ©thodes open et release
En gÃ©nÃ©ral, la mÃ©thode open rÃ©alise ces diffÃ©rentes opÃ©rations :

    * IncrÃ©mentation du compteur d'utilisation
    * ContrÃ´le d'erreur au niveau matÃ©riel
    * Initialisation du pÃ©riphÃ©rique
    * Identification du nombre mineur
    * Allocation et remplissage de la structure privÃ©e qui sera placÃ©e dans file->private_data

Le rÃ´le de la mÃ©thode release est tout simplement le contraire

    * LibÃ©rer ce que open a allouÃ©
    * Ã‰teindre la pÃ©riphÃ©rique
    * DÃ©crÃ©menter le compteur d'utilisation

Dans les noyaux actuels, le compteur d'utilisation est ajustÃ© automatiquement, pas besoin de s'en occuper.


#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>

MODULE_AUTHOR("djekmani4ever");
MODULE_DESCRIPTION("premier driver");
MODULE_SUPPORTED_DEVICE("none");
MODULE_LICENSE("none");

static int major = 254;

module_param(major, int, 0);
MODULE_PARM_DESC(major, "major number");

static ssize_t my_read_function(struct file *file, char *buf, size_t count, loff_t *ppos)
{
    printk(KERN_DEBUG "read()\n");
    return 0;
}

static ssize_t my_write_function(struct file *file, const char *buf, size_t count, loff_t *ppos)
{
    printk(KERN_DEBUG "write()\n");
    return 0;
}

static int my_open_function(struct inode *inode, struct file *file)
{
    printk(KERN_DEBUG "open()\n");
    return 0;
}

static int my_release_function(struct inode *inode, struct file *file)
{
    printk(KERN_DEBUG "close()\n");
    return 0;
}

static struct file_operations fops =
{
    read : my_read_function,
    write : my_write_function,
    open : my_open_function,
    release : my_release_function /* correspond a close */
};

static int __init mon_module_init(void)
{
    int ret;

    ret = register_chrdev(major, "mydriver", &fops);

    if(ret < 0)
    {
        printk(KERN_WARNING "Probleme sur le major\n");
        return ret;
    }

    printk(KERN_DEBUG "mydriver chargÃ© avec succÃ¨s\n");
    return 0;
}

static void __exit mon_module_cleanup(void)
{
    int ret;

    ret = unregister_chrdev(major, "mydriver");

    if(ret < 0)
    {
        printk(KERN_WARNING "Probleme unregister\n");
    }

    printk(KERN_DEBUG "mydriver dÃ©chargÃ© avec succÃ¨s\n");
}

module_init(mon_module_init);
module_exit(mon_module_cleanup);

Une fois le driver compilÃ© et chargÃ©, il faut tester son lien, avec le mode utilisateur, et plus particuliÃ¨rement les appels systÃ¨mes.

Tout d'abord, nous devons crÃ©er son fichier spÃ©cial : mknod /dev/mydriver c 254 0

Nous pouvons dÃ©sormais effectuer notre premier test : $ cat mydriver.c > /dev/mydriver

Maintenant vous pouvez vÃ©rifier en tapant dmesg, et voir les appels Ã  open(), write() et release().

Vous pouvez aussi crÃ©er votre propre programme qui ouvre le pÃ©riphÃ©rique et envoi une donnÃ©e, puis le referme.

Exemple:


#include <stdio.h>
#include <unistd.h>
#include <errno.h>

int main(void)
{
	int file = open("/dev/mydriver", O_RDWR);

	if(file < 0)
	{
		perror("open");
		exit(errno);
	}

	write(file, "hello", 6);

	close(file);

	return 0;
}


2.4. Allocation mÃ©moire
En mode noyau, l'allocation mÃ©moire se fait via la fonction kmalloc, la dÃ©sallocation, elle se fait via kfree.

Ces fonctions, sont trÃ¨s peu diffÃ©rentes des fonctions de la bibliothÃ¨que standard. La seule diffÃ©rence, est un argument supplÃ©mentaire pour la fonction kmalloc() : la prioritÃ©.

Cet argument peut-Ãªtre :

    * GFP_KERNEL : allocation normale de la mÃ©moire du noyau
    * GFP_USER : allocation mÃ©moire pour le compte utilisateur (faible prioritÃ©)
    * GFP_ATOMIC : alloue la mÃ©moire Ã  partir du gestionnaire d'interruptions


#include <linux/slab.h>

buffer = kmalloc(64, GFP_KERNEL);
if(buffer == NULL)
{
    printk(KERN_WARNING "problÃ¨me kmalloc !\n");
    return -ENONEM;
}
kfree(buffer), buffer = NULL;


2.5. MÃ©thodes read et write
Ces 2 mÃ©thodes renvoient le nombre d'octets, lus pour read et Ã©crits pour write.


static int buf_size = 64;
static char *buffer;

static ssize_t my_read_function(struct file *file, char *buf, size_t count, loff_t *ppos)
{
	int lus = 0;

	printk(KERN_DEBUG "read: demande lecture de %d octets\n", count);
	/* Check for overflow */
	if (count <= buf_size - (int)*ppos)
		lus = count;
	else  lus = buf_size - (int)*ppos;
	if(lus)
		copy_to_user(buf, (int *)p->buffer + (int)*ppos, lus);
	*ppos += lus;
	printk(KERN_DEBUG "read: %d octets reellement lus\n", lus);
	printk(KERN_DEBUG "read: position=%d\n", (int)*ppos);
	return lus;
}

static ssize_t my_write_function(struct file *file, char *buf, size_t count, loff_t *ppos)
{
	int ecrits = 0;
	int i = 0;

	printk(KERN_DEBUG "write: demande ecriture de %d octets\n", count);
	/* Check for overflow */
	if (count <= buf_size - (int)*ppos)
		ecrits = count;
	else  ecrits = buf_size - (int)*ppos;

	if(ecrits)
		copy_from_user((int *)p->buffer + (int)*ppos, buf, ecrits);
	*ppos += ecrits;
	printk(KERN_DEBUG "write: %d octets reellement ecrits\n", ecrits);
	printk(KERN_DEBUG "write: position=%d\n", (int)*ppos);
	printk(KERN_DEBUG "write: contenu du buffer\n");
	for(i=0;i<buf_size;i++)
		printk(KERN_DEBUG " %d", p->buffer[i]);
	printk(KERN_DEBUG "\n");
	return ecrits;
}

L'allocation mÃ©moire de buffer se fera lors du chargement du module.


buffer = kmalloc(buf_size, GFP_KERNEL);

Pour comprendre l'utilisation de buffer et de ppos, vous n'avez qu'Ã  faire plusieurs essais en mode utilisateur, avec par exemple des appels Ã  lseek.

Les fonctions copy_from_user et copy_to_user servent Ã  transfÃ©rer un buffer depuis/vers l'espace utilisateur.


copy_from_user(unsigned long dest, unsigned long src, unsigned long len);


copy_to_user(unsigned long dest, unsigned long src, unsigned long len);


2.6. MÃ©thode ioctl
Dans le cadre d'un pilote, la mÃ©thode ioctl sert gÃ©nÃ©ralement Ã  contrÃ´ler le pÃ©riphÃ©rique.

Cette fonction permet de passer des commandes particuliÃ¨res au pÃ©riphÃ©rique. Les commandes sont codÃ©es sur un entier et peuvent avoir un argument. L'argument peut-Ãªtre un entier ou un pointeur vers une structure.

Prototype :


int ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg);

Cette fonction est dÃ©finie dans <linux/fs.h>.

La fonction ioctl, cÃ´tÃ© utilisateur a ce prototype:


int ioctl(int fd, int cmd, char *argp);

Une fonction type ressemble Ã  :


int my_ioctl_function(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{
    int retval = 0;

    switch(cmd)
    {
        case ... : ... break;
        case ... : ... break;
        default : retval = -EINVAL; break;
    }
    return retval;
}

Ne pas oublier de rajouter cette entrÃ©e dans la structure file_operations.


static struct file_operations fops =
{
    read : my_read_function,
    write : my_write_function,
    open : my_open_function,
    release : my_release_function, /* correspond a close */
    ioctl : my_iotl_function;
};

Si la commande n'existe pas pour le pilote, l'appel systÃ¨me retournera EINVAL.

C'est au programmeur de choisir les numÃ©ros qui correspondent aux commandes.

Les numÃ©ros de commande, doivent Ãªtre uniques dans le systÃ¨me, afin d'Ã©viter l'envoi d'une commande au "mauvais" pÃ©riphÃ©rique, il existe pour cela une mÃ©thode sÃ»re pour choisir ses numÃ©ros.

Depuis la version 2.4, les numÃ©ros utilisent quatre champs de bits : TYPE, NOMBRE, SENS et TAILLE.

Le fichier header <asm/ioctl.h>, inclus dans <linux/ioctl.h>, dÃ©finit des macros qui facilitent la configuration des numÃ©ros de commande.

_IO(type, nr) oÃ¹ type sera le nombre magique (voir le fichier ioctl-number.txt)
_IOR(type, nr, dataitem) // sens de transfert : Lecture
_IOW(type, nr, dataitem) // sens de transfert : Ã‰criture
_IOWR(type, nr, dataitem) // sens de transfert : Lecture / Ã‰criture

Une dÃ©finition des numÃ©ros de commande ressemblera ainsi Ã  Ã§a :
driver.h


...
#define SAMPLE_IOC_MAGIC 't'
#define GETFREQ _IOR(SAMPLE_IOC_MAGIC, 0, int)
#define SETFREQ _IOW(SAMPLE_IOC_MAGIC, 1, int)

Ceci peut Ãªtre dÃ©fini dans un fichier .h qui sera inclus, dans le code du driver, et dans le code du programme pilotant le driver.
driver.c


#include "driver.h"
...

int my_ioctl_function(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{
    int retval = 0;

    switch(cmd)
    {
        case SETFREQ : ... break;
        case GETFREQ : ... break;
        default : retval = -EINVAL; break;
    }
    return retval;
}

utilisateur.c


#include "driver.h"
...

ioctl(file, SETFREQ, 100);
ioctl(file, SETACK, 10);


2.7. Gestion d'interruptions
Une interruption est un signal envoyÃ© par un matÃ©riel et qui est capable d'interrompre le processeur.

Notre pilote, met donc en place un gestionnaire d'interruptions pour les interruptions de son pÃ©riphÃ©rique.

Un gestionnaire d'interruptions est dÃ©clarÃ© par cette fonction : request_irq().

Il est libÃ©rÃ© par cette fonction : free_irq().

Les prototypes dÃ©finis dans <linux/sched.h> sont les suivants:


int request_irq(unsigned int irq,
		void (*handler) (int, void , struct pt_regs *),
		unsigned long flags /* SA_INTERRUPT ou SA_SHIRQ */,
		const char *device, void *dev_id);


void free_irq(unsigned int irq, void *dev_id);

Quand l'interruption qui a pour numÃ©ro irq se dÃ©clenche, la fonction handler() est appelÃ©e.

Le champ dev_id sert Ã  identifier les pÃ©riphÃ©riques en cas de partage d'IRQ (SA_SHIRQ).

La liste des IRQ dÃ©jÃ  dÃ©clarÃ©es est disponible dans /proc/interrupts.

Le gestionnaire d'interruptions peut-Ãªtre dÃ©clarÃ© Ã  2 endroits diffÃ©rents :

    * au chargement du pilote (module_init())
    * Ã  la premiÃ¨re ouverture du pilote par un programme (open()), il faut alors utiliser le compteur d'utilisation et dÃ©sinstaller le gestionnaire au dernier close().

Le driver peut dÃ©cider d'activer ou de dÃ©sactiver le suivi de ses interruptions. Il dispose de 2 fonctions pour cela, dÃ©finies dans <linux/irq.h>.


void enable_irq(int irq);
void disable_irq(int irq);

Cela peut-Ãªtre utile pour ne pas Ãªtre interrompu par une interruption, lors d'une interruption.

# milw0rm.com [2008-05-22]