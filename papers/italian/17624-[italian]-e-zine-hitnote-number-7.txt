     __  _____________   ______  ____________
    / / / /  _/_  __/ | / / __ \/_  __/ ____/
   / /_/ // /  / / /  |/ / / / / / / / __/
  / __  // /  / / / /|  / /_/ / / / / /___
 /_/ /_/___/ /_/ /_/ |_/\____/ /_/ /_____/ [0x07 :: Agust 2011]
                 Hackkappatoio Isn't The Name Of This Ezine

http://hitnote.gotdns.org :: http://unofficialhj.gotdns.org

******************************
* HITNOTE 0x07 :: Hard Times *
******************************


Perchè?

Ecco cosa penseranno molti a vedere questo numero e non vedendo quello passato.
Ed ecco la risposta:

sole, mare, vacanze, caldo, granita.

HITNOTE è in vacanza e anche alcuni di noi!

Alcuni appunto...noterete che questo numero è pressochè scritto da me:
d0ct0r questo perchè per qualche arcano motivo odio il sole il mare le
vacanze il caldo e la granita. Si la granita. Però quella all'anguria mi piace..
beh anche l'orzata non è male ma l'anguria...vendono lo sciroppo vero?
Non stiamo qui a divulgare.

Questo numero tratta praticamente solo di hardware dato che di software sono
una schiappa.
È pensato per chi vuole avvicinarsi all'elettronica e ne è intimorito.

Tu che ti avvicini all'elettronica, niente paura non morde!

...folgora.

Per il resto, il resto dello staff di HITNOTE vi augura un buon proseguimento
dell'estate e vi chiede un po di pazienza nei miei confronti :)


Io vi auguro buon inizio autunno..MUAHAHAH

Va bene basta preliminari passiamo al sodo.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Indice:

* 0x00 . JunkYard Fun
* 0x01 . La Fiaba Degli SMD
* 0x02 . Clip Rulez
* 0x03 . Occhio Al Robot
* 0x04 . Beyond The DLL
* 0x05 . Riddle
* 0x06 . Ringraziamenti


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


*********************
* 0x00 JunkYard Fun *
*********************

Immondizia. Così moltissimi chiamano un pc che non si accende più.
O magari una console che non legge bene i dischi, uno stereo da cui
non si sente più da una cassa o una vecchia TV.

Grazie a quei moltissimi possiamo assaporarci il vero gusto dell'arrangiarci
e del lusso a costo zero:

Per esperienza posso dire che la maggior parte delle volte in cui ci si
trova davanti a un hardware "rotto" sono cazzatine facilmente riparabili.

Prendiamo in esempio quelli precedentemente detti:

I pc. Ne ho trovati molti abbandonati in scantinati di amici e alla discarica
e tutti quelli che ho trovato avevano uno e un solo problema:
L'alimentatore. Vuoi il fusibile vuoi perchè avevano messo lo switch su
110 invece che su 230. Come ben dovremmo sapere tutti, se qualcosa non si
accende non è rotto tutto quanto ma la parte della sua alimentazione.
Mobo, RAM, Hard Disk, lettori vari, CPU, le varie schede e si anche
il case si possono benissimo recuperare e se ne vale la pena, si può aggiustare
o cambiare solo l'alimentatore e avremo un pc funzionante, magari nel caso
non troppo potente per farci un bel firewall.

Prendiamo invece in esempio una console che non legge più i dischi:

Ho avuto a che fare con la PS2 che vuoi il tempo, vuoi l'usura non aveva la
minima voglia di leggere alcun disco.
In questi casi ci sono 2 possibilità:

1) ricalibrare le rotelline del laser.
2) cercarne un altra "fusa" e cambiare lettore.

Io ho sempre optato per la prima ed ha sempre funzionato.

In quanto allo stereo, nel caso in esempio basterebbe aprirlo e riattaccare
uno dei due cavi dell'uscita audio che si è scollegato dallo spinotto.
Se si vuole fare una cosa fatta bene basta un pochino di sale e aceto
per togliere l'ossido dal cavo, ripulire per bene il cavo per poi stagnarlo
ed andarlo a saldare.
Ma assicuro che il 90% delle volte è semplicemente tagliato il cavo della
230 o è rotto il trasformatore.

Ma se è rotto il trasformatore?

Non è sempre un impresa trovare il modo di alimentare il nuovo arrivato:

Hanno sempre dei condensatori elettrolitici in alimentazione, basta vedere la
tensione di questi e partire dalla metà del valore della tensione.
Così potremmo trovare anche il modo di farlo portatile :)

Ho trovato poche settimane fa uno stereo del genere e da un piccolo ampli
a BC547 fatto da me sono passato al lusso del giradischi con AUX e
equalizzatore.

Dalla vecchia TV si può recuperare qualche bel transistor veloce e di potenza
appena prima del "flyback" ma attenzione, i tubi catodici sono MORTALI.
Per chi non sa cosa sia un tubo catodico, è quella sorta di imbuto di vetro
che forma lo schermo, per farla breve, se non viene scaricato: chi tocca muore.

Per i più navigati posso sempre consigliare un bel driver PWM sulla frequenza
di lavoro del flyback e una primaria avvolta a mano per dell'insano e letale
divertimento.**

**Ho usato un linguaggio incomprensibile a uno poco esperto in modo di evitare
che si folgori.


Ricordatevi che i modi esposti per trovare e riparare i vari danni, soprattutto
quelli del trasformatore rotto valgono per un sacco di oggetti.
Ma attenzione che i condensatori sono pericolosi poichè esplodono facilmente
e vanno scaricati prima di operare sul circuito.

Fate molta attenzione a non cortocircuitarne i piedini.


Morale della storia, tenete gli occhi aperti e puntati sulle discariche:
Anche gli smartphone e i nuovi laptop si romperanno.

byez!

d0ct0r


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


***************************
* 0x01 La Fiaba Degli SMD *
***************************

la fiaba degli SMD: per i comuni mortali

Odiosi, "inutili", stomachevoli!
Ecco come definivo gli SMD prima di innamorarmene...

Tutti coloro che si sono cimentati nell'hacking dell'hardware si sono
quasi sempre (specialente i più squattrinati) sentiti come bloccati da questi:
Magari le capacità ci sono, la voglia anche ma gli strumenti costano...
Quante schede lasciate da parte perchè si era già preso quel poco che
pensavamo offrissero!

Un giorno però, quando non disponevo più di una connessione a internet
da tempo e la noia dilagava, come un lampo mi arriva l'ideona:
una cosa stupida che anche una scimmia poteva capire, ma al quale pochi
sono arrivati.

Ecco cosa passava per la mia testa:

Ho la mia scheda fresca fresca di discarica.
Ha un sacco di SMD molti IC sono SMD hanno le stesse sigle di quelli
che ho già affrontato ma sono molto piccoli e mi piacerebbe dissaldarli
non ho il saldatore adatto...ma...Ho bisogno di caramelle gommose.

Hei!
Guardiamo meglio i piedini, sono messi li "con lo sputo", c'è veramente
poco stagno...Scommetto che se avessi le caramelle adatte...

Se avvicino una fiamma rompo l'IC, se lo forzo lo spacco e allora?
Vado a comprare le caramelle.

Ecco che arriva l'intuizione e le vostre istruzioni per un nuovo mondo!

Cosa serve:

-Carta abrasiva AKA carta vetro.
-Cacciavite piatto abbastanza piccolo ma robusto, con una buona impugnatura.
-Motore DC di quelli dei giocattoli o un trasformatore da buttare.
-Pinzette.
-Saldatore !!!va bene quello normale!!!
-Lente di ingrandimento.
-Pezzetto di dissipatore o di metallo molto conduttivo
-Stagno 60/40 (se non sapete cos'è usate quello, se sapete cos'è usate quello
che preferite).
-Caramelle gommose --- ESSENZIALI --- .

Cominciamo:

Prendiamo il cacciavite e affiliamolo molto bene con la carta abrasiva.
Deve arrivare al punto di essere tagliente.
Puliamo la punta e prendiamo la scheda da cui recuperare gli SMD.
Ora arriva il bello:
       ____
   ---|    |---          _  _________
   ---|    |---  _______| |/         \
   ---|    |--- |_______|   --------- |
   ---|____|---         |_|\_________/

Posizionate il cacciavite nello stesso senso in figura e piano piano
fate struciolare lo stagno dall'esterno all'interno in modo di far scivolare
i piedini al di sopra della punta.

ATTENZIONE:
Mentre fate questa manovra tenete l'IC e fate un piedino alla volta partendo
dai centrali di entrambi i lati per poi andare sull'esterno.
Ad esempio:

       ____
 8 ---|    |--- 1   Partiremo da 2 poi andremo su 3, 7, 6
 7 ---|    |--- 2   poi passeremo a 5, 1
 6 ---|    |--- 3   per finire con 8 e 4.
 5 ---|____|--- 4

OCCHIO ALLE DITA: c'è bisogno che linko la foto della mia cicatrice? XD

Se avrete eseguito correttamente la manovra vi ritroverete l'IC in mano
senza bisogno di forzarlo e le dita ancora attaccate alla mano.

A questo punto bisogna raddrizzare i piedini, aiutatevi con il cacciavite
prima raddrizzandoli verso l'esterno poi distanziandoli l'uno dall'altro.

Una volta fatto ciò recuperate l'avvolgimento del motore DC o del trasformatore
prendete i cavi sottili poi togliete lo smalto alle estremità e stagnate.

Una volta fatto questo mettete l'IC sul pezzo di dissipatore e tenetelo con la
pinzetta per poi bloccare la pinzetta in modo che il metallo aderisca al
componente che andremo a operare.
In questo modo eviteremo il surriscaldamento e quindi la distruzione del
piccino!

Se avete la terza mano è meglio, ne avrete una in più per le caramelle!

Fatto ciò, prendiamo il cavo sottile ricavato dal motore o dal trasformatore
tagliamo degli spezzoni lunghi circa 3 centimetri e togliamo lo smalto
con la carta abrasiva per poi stagnarli alle estremità.

Ora prendiamo la lente e avviciniamo il cavo ad un piedino saldando
pazientemente non il cavo al piedino ma il piedino al cavo!
Ebbene è questo il trucco, il piedino va scaldato poco.
Per essere più comodi consiglio di seguire lo stesso ordine che ho precedente-
mente detto per dissaldarlo: 2, 3, 7, 6, 5, 1, 8, 4.

ora abbiamo un IC SMD pronto per essere usato su millefori o come un normale
IC!

Ricordate solo questo: Non scaldare l'IC e tutto andrà bene.

Con questa tecnica posso dire per esperienza che si sporca molto meno
e si consuma pochissimo; senza parlare poi della figata che esce
se fate un bel lavoro pulito! :)

Quando ho trovato questa tecnica mi si è aperto un mondo, spero sia utile
anche a voi!

byez!

d0ct0r


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


*******************
* 0x02 Clip Rulez *
*******************

Già che ci siamo e che questo numero è dedicato ai più volenterosi
che vogliono avvicinarsi al magnifico mondo dell'elettronica voglio
parlare di una cosa spesso considerata inutile e di scarso interesse.
Le graffette.

No non sono impazzito del tutto, è già successo tempo fa!^^

Parlo proprio delle graffette fermacarta, anche conosciute come paper clips.

"Ma cosa centrano con l'elettronica?" vi starete chiedendo..ci arriviamo ora.

pensate a quando dovete togliere un circuito integrato dal suo zoccoletto,
è cosa difficile senza stortare i piedini.
Si in commercio ci sono pinze adatte ma chi ha appena cominciato, molto spesso
non sa dove reperirle o costano troppo per utilizzarle magari 2 volte ogni
6 mesi.

Ora vi insegnerò come costruire questa pinza usando delle graffette grandi.


 Prendete la graffetta e lavoratela un po: dobbiamo renderla
 dritta il più possibile.

 Trucchetto: per renderla ben dritta seguite la curvatura con una pinza.

 Una volta resa dritta trovate la metà e segnatela, poi misurate 2 centimetri
 dalle estremità e piegate nel senso in cui le parti "tagliate"
 si vedano in faccia.

 Piegatele fino a 90°.

 Ora piegate la metà sempre nel senso che vada a chiudere come a formare un
 triangolo con le estremità precedentemente piegate, a questo punto si
 può già intuire come andrà ad operare la nostra pinzetta.

 Per un utilizzo più comodo e preciso schiacciamo bene con una pinza il centro
 appena piegato in modo che diventi molto stretto; a questo punto le estremità
 piegate a 90° dovrebbero toccarsi i "talloni" come due L vicine dopo essersi
 incrociate.

 Ora misuriamo mezzo centimetro circa dall'estremità del centro che abbiamo
 stretto.

 Misuriamolo sia da una parte che dall'altra e poi pieghiamo verso l'alto e poi
 verso il basso in modo di formare una S.

 Facciamolo sia da una parte che dall'altra.
 Il risultato sarà una m a testa in giù scritta in corsivo con le estremità che
 vanno verso il basso a chiudersi grazie a quelle estremità che
 avevamo piegato a 90°.


 Ora pinziamo la m rovesciata e facciamola inclinare in maniera orizzontale.

 Con la struttura ottenuta noteremo che se prendiamo per la m la pinza e
 forziamo le due L che andranno a pinzare l'IC si fa una leva che spingerà
 l'IC a sollevarsi dallo zoccolo.

 Gli zoccoli hanno dei buchini in cui i piedini della nostra pinza possono
 entrare con facilità.

 Una volta inserita sotto all'IC prendiamo la parte interna alla pinza della m
 con l'indice e quella esterna col pollice e con cautela tiriamo su l'IC.

 Noterete come quel lavoro a m che sembrava inutile e i piedini così lunghi,
 giochino un lavoro di leve che rende il compito quasi liscio come l'olio :)

 lo so come guida è un po macchinosa ma il risultato è testato personalmente.


 Ho costruito io stesso altre due pinze scrivendo questo articolo quindi spero
 si capisca.



Buon lavoro :D

byez!

d0ct0r


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


************************
* 0x03 Occhio Al Robot *
************************

In questo articolo parlerò del modo più semplice e ahimè ne son colpevole,
meno onorevole che esista per dotare di visione il robot o il pc.

I motivi per cui dotare di vista il robot sono molti e gli scopi altrettanti.
Ad esempio per fotografare in automatico le facce di chi passa vicino al server
e salvarle da qualche parte oppure per far risolvere il cubo di rubik
al pc o ancora per la navigazione autonoma del robot superfigo che abbiamo
fatto.

Il tempo è denaro dicono. Alcuni dicono che è relativo, io dico che senz'altro
è risparmiato usandolo bene.

Come ho già detto è poco onorevole e alcuni potrebbero sentirsi disgustati
almeno quanto io mi sentirei disgustato da un tizio che usa un Arduino
per accendere 2 led.

Perdonatemi maghi del software ma questo articolo è pensato per quegli
hardwareisti cocciuti a cui proprio non entra in testa il C.

Basta chiacchere, il metodo è semplice:
Si tratta di un ambiente di sviluppo visuale open di nome Harpia ed è
disponibile anche nei repo di ubuntu ma pochi lo conoscono io l'ho trovato
un modo pratico e veloce per dare la vista al mio piccolino.

Questo programma non è altro che un sistema di drag & drop con varie "caselle"
ognuna con la sua funzione tra cui prelevare immagini, confrontarle e
detectare facce, colori, linee o cerchi le quali possono essere combinate
in stile diagramma di flusso per finire in un comando nel terminale o in un
video o una fotografia finale.
Il frutto del lavoro sarà un codice C generato da Harpia che potrà essere
modificato a piacimento e compilato.

Ovviamente serve opencv e tutte le altre dipendenze.

Prendiamo in esempio il programma che ho ""fatto io"":

Ho fatto un New Image che riprende da /dev/video0, lo passa al filtro
Canny che rende i bordi delle cose bianchi e il resto nero, dopodichè
passa a Detect Lines che trova le linee e le marca in rosso poi
questo filmato così lavorato va a Detect Color che cerca il rosso.

A questo punto ho creato 3 nuovi rettangoli (Create Rect) e li ho messi
uno a destra, uno a sinistra e uno al centro rispetto alle dimensioni del
filmato poi ho messo altri 3 Check Point, che vanno a controllare se il
Pt (point) trovato da Detect Color(ma si può usare qualsiasi feature detector)
è dentro uno dei rettangoli da me creati.

A questo punto all'uscita di Check Point ho messo tre Run Command e ho spuntato
la voce "esegui solo se vero" in modo che quando il centro è all'interno
di uno dei miei rettangoli Check Point lo veda e esegua il comando che gli ho
detto a seconda di quale rettangolo sia coinvolto.

Come comando ho messo un semplice echo "d" per destra, echo "s" per sinistra
e echo "u" per centro.

Una volta compilato mi printa sullo schermo la posizione del centro della sua
"attenzione" e tramite un semplice script o un programma posso interpretarlo

ad esempio:

codicecompilato | script.py

facendo leggere a script.py l'input da tastiera riceverà "s" "d" o "u"
e lo potremo far reagire di conseguenza :)

Ad esempio io mando quei caratteri alla porta seriale e ci guido un Arduino
che fa il lavoro sporco.

Spero sia utile a qualcuno che vuole cominciare e poi si sa, vuoi sempre
di più e si finisce per spiluccare un po di qua e un po di la e
si finisce per impararlo veramente a sto C.


byez!

d0ct0r


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


***********************
* 0x04 Beyond The DLL *
***********************

*****
Intro
*****

AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

L'urlo della giungla? No.
È passato fuori di testa? No.

Queste AAAAAAAAAAA sono state messe da r0b0t82 per motivi ancora sconosciuti.
A me piace pensare che sia lo sbadiglio di quando si è avegliato dal letargo,
si è visto un po trippone e ha finalmente deciso di aiutarmi con questo numero.
Ovviamente si scherza, non lo ha deciso, è stato obbilgato!^^

Ovviamente è spuntato fuori con un articolo software.
Eccovelo.

*****************
fine intro by d0c
*****************

HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\Notify

Forse solo pochi di voi avran già sentito questa chiave: è uno dei tanti punti
dove windows carica dei files all'avvio. Più precisamente, delle DLL.
...e le DLL non vengon mostrate nel fantomatico Task Manager.. :D

Dunque, in questa chiave va creata una sottochiave con un nome a nostro
piacimento. Dentro a questa van creati i seguenti valori:

Asynchronous <= REG_DWORD: 0 o 1: se impostato a 1, la DLL passa in background
                                  e l'utente non si accorge di essa.
                                  Impostando 0, windows aspetta la fine
                                  dell'esecuzione prima di poter continuare.
Impersonate  <= REG_DWORD: 0 o 1 anche questa: Se impostata a 1, alcune chiamate
                                               verranno eseguite con il livello
                                               dell'utente attivo. Se 0, esse
                                               verranno eseguite tramite utente
                                               'System' :)

DllName      <= REG_EXPAND_SZ: Percorso della DLL

e le successive, tutte REG_SZ dove andrà scritto il nome della funzione della
DLL che le gestirà:

Lock             : blocco dello schermo
UnLock           : sblocco dello schermo

LogOn            : login dell'utente
LogOff           : logout dell'utente
StartShell       : dopo login dell'utente

Startup          : avvio del WinLogon
Shutdown         : spegnimento del pc

StartScreenSaver : avvio dello screensaver
StopScreenSaver  : chiusura dello screensaver

Naturalmente non sono tutti obbligatori :)

Ora viene la parte della DLL. Naturalmente facciamo un esempio in C.
Basta creare un nuovo progetto in Visual Studio e...


__declspec( dllexport ) void _stdcall lol(){
  //qui il codice da eseguire chiamando la funzione 'lol'
}

Dopodichè basta aggiungere un file delle definizioni(nomedll.def) e collegarlo
dalle impostazioni progetto, sotto 'Linker'->'input'->'File definizioni prog.'
con il seguente contenuto:

---------------------
LIBRARY nomelib;
EXPORTS
funzione1
funzione2
---------------------


nel nostro caso:
---------------------
LIBRARY nomelib;
EXPORTS
lol
---------------------

Dopodichè, se volete provare al volo qualche funzione della libreria,
da esegui basta dare:
'rundll32 nomelib.dll lol' <= questo esegue la funzione 'lol' di 'nomelib.dll'

Che dire, buon divertimento! ;)

r0b0t82


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


******************
* 0x05 :: Riddle *
******************

Il riddle di questo numero è:

rosso-marrone marrone-marrone-bianco bianco-viola marrone-marrone-verde

marrone-nero-bianco marrone-rosso-marrone marrone-nero-rosso marrone-nero-verde

marrone-marrone-giallo marrone-marrone-verde marrone-marrone-blu

marrone-marrone-bianco marrone-marrone-marrone marrone-marrone-giallo

marrone-nero-nero

Aspettiamo risposte a mailhitnote@gmail.com ;)

d0ct0r


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


**************************
* 0x06 :: Ringraziamenti *
**************************

Per questo numero si ringrazia, in ordine alfabetico:
d0ct0r(99.9999%), r0b0t82, black, Doch

La ezine HITNOTE è a cura dell' Unofficial HJ Forum:
http://unofficialhj.gotdns.org

Il sito ufficiale di HITNOTE è:
http://hitnote.gotdns.org

Insultateci, criticateci, inviateci tutto quel che vi passa per la testa a:
mailhitnote@gmail.com

Il canale IRC su cui ci incontriamo è:
#hj @ unofficialhj.gotdns.org:6667 [6697 via SSL]
#hjssl @ unofficialhj.gotdns.org:6697 [solo via SSL]


HITNOTE 0x07 [Agust 2011] -- Released under Creative Commons